// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Public/Troups/TroupRole.h"
#include "Public/StarPowers/CardType.h"
#include "StarPower.h"

#include "BoardConquestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ABoardConquestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABoardConquestGameModeBase();

protected:
	virtual void BeginPlay() override;


#pragma region SpawnableTroups
protected:
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> chessmanToSpawn;
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> rookToSpawn;
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> bishopToSpawn;
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> knightToSpawn;
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> queenToSpawn;
	UPROPERTY(EditAnywhere, Category = "TroupsToSpawn")
		TSubclassOf<ATroup> kingToSpawn;

public:
	TArray<TTuple<TSubclassOf<ATroup>, ETroupRole>> getAllBlueprintTroups();
#pragma endregion


#pragma region SpawnableCards
protected:
	UPROPERTY(EditAnywhere, Category = "CardsToSpawn")
		TSubclassOf<AStarPower> instantDeath;
	UPROPERTY(EditAnywhere, Category = "CardsToSpawn")
		TSubclassOf<AStarPower> instantClone;

public:
	TArray<TTuple<TSubclassOf<AStarPower>, ECardType>> getAllBlueprintCards();
#pragma endregion
};
