// Copyright Epic Games, Inc. All Rights Reserved.


#include "BoardConquestGameModeBase.h"

#include "Map/InitMap.h"
#include "Map/BoardStyle.h"
#include "Map/BoardType.h"

ABoardConquestGameModeBase::ABoardConquestGameModeBase()
{

}

void ABoardConquestGameModeBase::BeginPlay()
{
	Super::BeginPlay();

}

TArray<TTuple<TSubclassOf<ATroup>, ETroupRole>> ABoardConquestGameModeBase::getAllBlueprintTroups()
{
	TArray<TTuple<TSubclassOf<ATroup>, ETroupRole>> result;
	result.Add(MakeTuple(chessmanToSpawn, ETroupRole::CHESSMAN));
	result.Add(MakeTuple(rookToSpawn, ETroupRole::ROOK));
	result.Add(MakeTuple(bishopToSpawn, ETroupRole::BISHOP));
	result.Add(MakeTuple(knightToSpawn, ETroupRole::KNIGHT));
	result.Add(MakeTuple(queenToSpawn, ETroupRole::QUEEN));
	result.Add(MakeTuple(kingToSpawn, ETroupRole::KING));
	return result;
}

TArray<TTuple<TSubclassOf<AStarPower>, ECardType>> ABoardConquestGameModeBase::getAllBlueprintCards()
{
	TArray<TTuple<TSubclassOf<AStarPower>, ECardType>> result;
	result.Add(MakeTuple(instantDeath, ECardType::INSTANTDEATH));
	result.Add(MakeTuple(instantClone, ECardType::INSTANTCLONE));
	return result;
}