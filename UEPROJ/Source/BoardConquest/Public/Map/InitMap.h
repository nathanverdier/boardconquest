// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "Engine/World.h"
#include "Tile.h"
#include "BoardType.h"
#include "BoardStyle.h"
#include "Troups/TroupRace.h"
#include "StarPower.h"
#include "StarPowers/CardType.h"
#include "StarPowers/InstantDeath.h"
#include "StarPowers/InstantClone.h"
#include "StarPowers/Bramble.h"
#include "StarPowers/DestroyTerrain.h"
#include "StarPowers/RockBind.h"
#include "StarPowers/RankLoss.h"
#include "StarPowers/TrialPromotion.h"

#include "InitMap.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API AInitMap : public AActor
{
	GENERATED_BODY()

#pragma region BoardParameters
protected:
	TSubclassOf<ATile> tileToSpawn;
	float tileSize;

	int boardWidth, boardLength;
	EBoardType boardType;
	EBoardStyle boardStyle;
#pragma endregion


#pragma region ArmyParameters
protected:
	TSubclassOf<ATroup> chessmanToSpawn;
	TSubclassOf<ATroup> rookToSpawn;
	TSubclassOf<ATroup> bishopToSpawn;
	TSubclassOf<ATroup> knightToSpawn;
	TSubclassOf<ATroup> queenToSpawn;
	TSubclassOf<ATroup> kingToSpawn;
#pragma endregion


#pragma region Initializing
private:
	bool mapInitializeFunctionHasBeenCalled = false;
	bool armyInitializeFunctionHasBeenCalled = false;
	bool cardsInitializeFunctionHasBeenCalled = false;

public:
	void initializeMapCreatorComponent(UWorld* newWorld, EBoardType myNewBoardType, EBoardStyle myNewBoardStyle, TSubclassOf<ATile> myNewTileToSpawn, float myNewTileSize);
	void initializeArmyCreatorComponent(UWorld* newWorld, EBoardType myNewBoardType, TArray<TTuple<TSubclassOf<ATroup>, ETroupRole>> troups);
	void initializeCardsCreatorComponent();
#pragma endregion


#pragma region AboutWorld.h
protected:
	UWorld* world;
#pragma endregion


#pragma region CreatingAndSpawningBoard
private:
	ATile* spawnTile(FVector spawnLocation);
	void setColorToBoard(TArray<TArray<ATile*>> board);

public:
	TArray<TArray<ATile*>> createAndSpawnBoard();
#pragma endregion


#pragma region CreatingAndSpawningTroups
private:
	TArray<TTuple<TTuple<int, int>, ETroupRole>> armyComposition(FString player);
	TArray<TTuple<TTuple<int, int>, ETroupRole>> armyCompositionOneOnly(FString player);
	TArray<TTuple<TTuple<int, int>, ETroupRole>> armyCompositionClassic(FString player);
	TArray<TTuple<TTuple<int, int>, ETroupRole>> armyCompositionDouble(FString player);

	TArray<ATroup*> spawnTroupsOnBoard(TArray<TTuple<TTuple<int, int>, ETroupRole>> composition, TArray<TArray<ATile*>> board, ETroupRace troupRace);
	ATroup* spawnTroupOnTile(ETroupRole troupRole, ATile* tile);

public:
	TArray<ATroup*> createAndSpawnPlayerArmy(FString player, ETroupRace troupRace, TArray<TArray<ATile*>> board);
#pragma endregion 

#pragma region CreatingAndSpawningCards
private:
	AStarPower* spawnACard(ECardType cardType, FVector worldLocation);

public:
	TArray<AStarPower*> createAndSpawnPlayerCards(FString player);
#pragma endregion 
};
