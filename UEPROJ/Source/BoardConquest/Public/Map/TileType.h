#pragma once


UENUM(BlueprintType)
enum class ETileType : uint8
{
    DEFAULT = 0 UMETA(DisplayName = "DEFAULT"),
    WHITE = 1 UMETA(DisplayName = "WHITE"),
    BLACK = 2 UMETA(DisplayName = "BLACK"),
    LIGHTBROWN = 3 UMETA(DisplayName = "LIGHTBROWN"),
    DEEPBROWN = 4 UMETA(DisplayName = "DEEPBROWN")
};