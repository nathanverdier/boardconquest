#pragma once


UENUM(BlueprintType)
enum class EBoardStyle : uint8
{
    DEFAULT = 0 UMETA(DisplayName = "DEFAULT"),
    CLASSIC = 1 UMETA(DisplayName = "CLASSIC"),
    BROWN = 2 UMETA(DisplayName = "BROWN")
};