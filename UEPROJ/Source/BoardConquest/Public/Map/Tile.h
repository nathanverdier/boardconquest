// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Materials/Material.h"
#include "Materials/Material.h"
#include "Map/TileType.h"
#include "Troup.h"

#include "Tile.generated.h"

UCLASS()
class BOARDCONQUEST_API ATile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma region TreeComponent
protected:
	UPROPERTY(EditAnywhere)
		class UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* StaticMesh;

private:
	void setUpTreeComponents();

public:
	UStaticMeshComponent* getStaticMesh();
#pragma endregion

#pragma region CustomMaterial
protected:
	UPROPERTY(BlueprintReadOnly)
		ETileType type;
	UPROPERTY(EditAnywhere, Category = "Custom Materials")
		UMaterialInstance* defaultMaterial;
	UPROPERTY(EditAnywhere, Category = "Custom Materials")
		UMaterialInstance* whiteMaterial;
	UPROPERTY(EditAnywhere, Category = "Custom Materials")
		UMaterialInstance* blackMaterial;
	UPROPERTY(EditAnywhere, Category = "Custom Materials")
		UMaterialInstance* lightBrownMaterial;
	UPROPERTY(EditAnywhere, Category = "Custom Materials")
		UMaterialInstance* deepBrownMaterial;

private:
	void setMaterial();
	void setWhiteMaterial();
	void setBlackMaterial();
	void setLightBrownMaterial();
	void setDeepBrownMaterial();
	void setDefaultMaterial();

public:
	void setType(ETileType newTileType);
#pragma endregion


#pragma region HandleOccupyingTroup
private:
	ATroup* occupyingTroup;

public:
	bool isAvailable = true;

public:
	ATroup* getOccupyingTroup();
	bool isOccupied();
	void setOccupyingTroup(ATroup* newOccupyingTroup);
#pragma endregion


#pragma region SpawnableActors
protected:
	UPROPERTY(EditAnywhere, Category = "Spawnables")
		TSubclassOf<AActor> BrambleToSpawnClass;
	AActor* activeBramble;

public:
	void spawnBramble();
	void destroyBramble();
#pragma endregion


#pragma region Others
public:
	FVector worldLocation;
#pragma endregion
};
