#pragma once


UENUM(BlueprintType)
enum class EBoardType : uint8
{
    DEFAULT = 0 UMETA(DisplayName = "DEFAULT"),
    CLASSIC = 1 UMETA(DisplayName = "CLASSIC"),
    DOUBLE = 2 UMETA(DisplayName = "DOUBLE"),
    ONEONLY = 3 UMETA(DisplayName = "ONEONLY")
};