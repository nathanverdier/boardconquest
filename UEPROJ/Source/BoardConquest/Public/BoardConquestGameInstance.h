// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "Map/BoardStyle.h"
#include "Map/BoardType.h"
#include "Map/Tile.h"
#include "Troup.h"
#include "Troups/TroupRace.h"
#include "StarPowers/CardType.h"
#include "StarPower.h"

#include "BoardConquestGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API UBoardConquestGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UBoardConquestGameInstance();

#pragma region AboutMenu
public:
	FString Joueur1GamerTag;
	FString Joueur2GamerTag;
	ETroupRace Joueur1Race = ETroupRace::KNIGHT;
	ETroupRace Joueur2Race = ETroupRace::MONSTER;
	EBoardStyle myTileColor;
	EBoardType myDimensionBoard;
	UPROPERTY(BlueprintReadWrite)
		TArray<ECardType> Player1HandTypes = { ECardType::INSTANTDEATH, ECardType::TRIALPROMOTION, ECardType::RANKLOSS };
	UPROPERTY(BlueprintReadWrite)
		TArray<ECardType> Player2HandTypes = { ECardType::INSTANTCLONE, ECardType::DESTROYTERRAIN, ECardType::ROCKBIND };
	UPROPERTY(BlueprintReadWrite)
		FName meteoActu;
	UPROPERTY(BlueprintReadWrite)
		bool isRaining;

public:
	UFUNCTION(BlueprintCallable)
		void menuSetting(FString GamerTagJ1, FString GamerTagJ2, ETroupRace Joueur1RacePawn, ETroupRace Joueur2RacePawn, EBoardStyle newTileColor, EBoardType myDimensionBoard);
	UFUNCTION(BlueprintCallable)
		void resetGameInstance();
	UFUNCTION(BlueprintCallable)
		EBoardType getBoardDimension();
#pragma endregion


#pragma region GameStatus
public:
	TArray<TArray<ATile*>> board;
	TArray<ATroup*> player1Army;
	TArray<ATroup*> player2Army;
	UPROPERTY(BlueprintReadOnly)
		TArray<AStarPower*> player1Hand;
	UPROPERTY(BlueprintReadOnly)
		TArray<AStarPower*> player2Hand;
#pragma endregion

#pragma region Meteo
public:
	UPROPERTY(BlueprintReadWrite, Category = "TimeLeftForThisDays")
		int timeInDay;
	UPROPERTY(BlueprintReadWrite, Category = "RandomDays")
		bool actuRandom = true;
	UPROPERTY(BlueprintReadWrite, Category = "RandomDays")
		bool buttonRandomActivate = false;
#pragma endregion
};