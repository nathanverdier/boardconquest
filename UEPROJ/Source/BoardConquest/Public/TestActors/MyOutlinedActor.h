// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyOutlinedActor.generated.h"

UCLASS()
class BOARDCONQUEST_API AMyOutlinedActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyOutlinedActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MeshComponent;

#pragma region Outline
private:
	bool IsOutlined;

public:
	void SetOutline(bool value);
#pragma endregion

};
