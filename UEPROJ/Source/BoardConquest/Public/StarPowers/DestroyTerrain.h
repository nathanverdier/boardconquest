// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"
#include "BoardConquestGameInstance.h"
#include "DestroyTerrain.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ADestroyTerrain : public AStarPower
{
	GENERATED_BODY()

public:
	ADestroyTerrain();

#pragma region Power
private:
	int moveLeft = 3;

public:
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
#pragma endregion
};
