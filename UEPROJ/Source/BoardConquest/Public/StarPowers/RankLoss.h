// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../StarPower.h"
#include "RankLoss.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ARankLoss : public AStarPower
{
	GENERATED_BODY()

public:
	ARankLoss();

protected:
	virtual void usePower(AActor* target, FString actualPlayer) override;
	virtual TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
};
