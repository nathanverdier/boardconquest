// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"

#include "BoardConquestGameInstance.h"
#include "../BoardConquestGameModeBase.h"

#include "TrialPromotion.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ATrialPromotion : public AStarPower
{
	GENERATED_BODY()
	
public:
	ATrialPromotion();

#pragma region UsePower
public:
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
#pragma endregion


#pragma region TargettedActorsStatus
private:
	ATroup* troupTransformed;
	ETroupRole troupTransformedOldRole;
	FString troupTransformedOwner;

public:
	void endTargettedActorsStatus() override;
#pragma endregion
};
