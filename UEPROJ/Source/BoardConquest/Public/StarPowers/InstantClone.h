// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"
#include "BoardConquestGameInstance.h"
#include "Troups/TroupRole.h"
#include "InstantClone.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API AInstantClone : public AStarPower
{
	GENERATED_BODY()

public:
	AInstantClone();

public:
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer);
	
private:
	bool findATileForDuplication(const int line, const int col, UBoardConquestGameInstance* GI, FString player, int* validLine, int* validCol);
	ATroup* SpawnATroupOnTile(const int line, const int col, UBoardConquestGameInstance* GI, ATroup* troupToDuplicate);
};
