// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"
#include "BoardConquestGameInstance.h"
#include "Bramble.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ABramble : public AStarPower
{
	GENERATED_BODY()

public:
	ABramble();


#pragma region PowerUsage
public:
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
#pragma endregion


#pragma region TargettedActorsStatus
private:
	int tileLigne, tileColone;
	TArray<ATile*> myTilesOut;

public:
	void endTargettedActorsStatus() override;
#pragma endregion
};
