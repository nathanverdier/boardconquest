// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"
#include "BoardConquestGameInstance.h"
#include "InstantDeath.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API AInstantDeath : public AStarPower
{
	GENERATED_BODY()


public:
	AInstantDeath();

#pragma region Power
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
#pragma endregion
};
