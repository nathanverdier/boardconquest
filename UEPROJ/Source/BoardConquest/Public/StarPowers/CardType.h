#pragma once


UENUM(BlueprintType)
enum class ECardType : uint8
{
    INSTANTDEATH = 0 UMETA(DisplayName = "InstantDeath"),
    INSTANTCLONE = 1 UMETA(DisplayName = "InstantClone"),
    BRAMBLE = 2 UMETA(DisplayName = "Bramble"),
    DESTROYTERRAIN = 3 UMETA(DisplayName = "DestroyTerrain"),
    ROCKBIND = 4 UMETA(DisplayName = "RockBind"),
    RANKLOSS = 5 UMETA(DisplayName = "RankLoss"),
    TRIALPROMOTION = 6 UMETA(DisplayName = "TrialPromotion")
};