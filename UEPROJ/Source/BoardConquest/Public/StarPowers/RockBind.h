// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StarPower.h"
#include "BoardConquestGameInstance.h"
#include "RockBind.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ARockBind : public AStarPower
{
	GENERATED_BODY()

public:
	ARockBind();

#pragma region UsePower
public:
	void usePower(AActor* target, FString actualPlayer) override;
	TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer) override;
#pragma endregion


#pragma region TargettedActorsStatus
private:
	ATroup* troupTargetted;

public:
	void endTargettedActorsStatus() override;
#pragma endregion
};
