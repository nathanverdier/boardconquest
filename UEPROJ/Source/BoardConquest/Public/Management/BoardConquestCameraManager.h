// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoardConquestCamera.h"
#include "GameFramework/Actor.h"
#include "BoardConquestCameraManager.generated.h"

UCLASS()
class BOARDCONQUEST_API ABoardConquestCameraManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoardConquestCameraManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma region SwitchView
public:
	void switchView(ABoardConquestCamera* cameraPlayer1, float time);
#pragma endregion


};
