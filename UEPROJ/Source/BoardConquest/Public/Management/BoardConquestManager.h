// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Map/Tile.h"
#include "Troup.h"
#include "Movement/PrinterMovementActor.h"
#include "GameFramework/PlayerController.h"
#include "Movement/TroupMovementComponent.h"
#include "BoardConquestGameInstance.h"
#include "BoardConquestCamera.h"
#include "Management/BoardConquestCameraManager.h"

#include "BoardConquestManager.generated.h"

UCLASS()
class BOARDCONQUEST_API ABoardConquestManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoardConquestManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


#pragma region GameInstance
private:
	UBoardConquestGameInstance* myGI;

private:
	void setUpGameInstance();
#pragma endregion


#pragma region HandleSelectedActors
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Selected Actors")
		ATile* selectedTile;
	UPROPERTY(BlueprintReadOnly, Category = "Selected Actors")
		ATroup* selectedTroup;
	APlayerController* firstPlayerController;

private:
	void onLeftClick();

public:
	void setSelectedTile(ATile* myNewSelectedTile);
	void setSelectedTroup(ATroup* myNewSelectedTroup);
	UFUNCTION(BlueprintCallable)
		ATroup* getSelectedTroup();
#pragma endregion


#pragma region DisplayingOutlines
protected:
	APrinterMovementActor* myDisplayer;
	bool displayerInitialized = false;

private:
	void outlineActor(AActor* actorToOutline, bool value);
	void initializeDisplayer();
#pragma endregion


#pragma region HandleMovement
private:
	ATroupMovementComponent* possibleMovementHandler;
	TArray<ATile*> currentPossibleMovement;

private:
	void moveTroupTo(ATroup* troupToMove, ATile* tileTargetted);
	bool movementAllowed(ATile* tileTargetted);
	void transformChessmanInQueen(ATroup** troupToTransform);
#pragma endregion


#pragma region Cards
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Selected Actors")
		AStarPower* selectedStarPower;
	UPROPERTY(BlueprintReadOnly, Category = "Selected Actors")
		TArray<AActor*> possibleActorsToUseStarPowerOn;

private:
	bool checkIfStarPowerCanBeUsedOn(AActor* actorTargetted);
	void useActiveStarPowerOn(AActor* actorTargetted);

public:
	UFUNCTION(BlueprintCallable)
		bool isACardActive();
	UFUNCTION(BlueprintCallable)
		void cardClicked(AStarPower* cardReferencedByUIClicked);
	UFUNCTION(BlueprintCallable)
		void cardCancelled();
	UFUNCTION(BluePrintCallable)
		bool checkIfACardIsReadyToUse(ECardType cardTypeTaregetted, FString Player);
#pragma endregion


#pragma region CameraManager
	ABoardConquestCameraManager* cameraManager;
#pragma endregion


#pragma region HandlePlayer
private:
	FString playerTurn;

private:
	void playTurn();

public:
	UFUNCTION(BlueprintCallable)
		FString getPlayerTurn();
	UFUNCTION(BlueprintCallable)
		FString getPlayerNameTurn();
#pragma endregion


#pragma region SetViewCurrentUser
public:
	UFUNCTION(BlueprintCallable, Category = "SetViewCam1")
		void switchPlayerView();
	UFUNCTION(BlueprintCallable, Category = "SetViewCam2")
		void setViewCam2();
	UFUNCTION(BlueprintCallable, Category = "SetViewCam3")
		void setViewCam3();
#pragma endregion


#pragma region HandleCamera
private:
	ABoardConquestCamera* cameraPlayer1;
	ABoardConquestCamera* camera2Player1;
	ABoardConquestCamera* camera3Player1;
	ABoardConquestCamera* cameraPlayer2;
	ABoardConquestCamera* camera2Player2;
	ABoardConquestCamera* camera3Player2;

private:
	void initializePlayerView();
#pragma endregion


#pragma region End
protected:
	bool gameIsOver = false;

public:
	UFUNCTION(BlueprintCallable)
		bool isThegameOver();
#pragma endregion
};
