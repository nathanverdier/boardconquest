// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Map/Tile.h"
#include "Templates/Tuple.h"

/**
 * 
 */
class BOARDCONQUEST_API ATroupMovementComponent : public AActor
{
public:
	ATroupMovementComponent();
	~ATroupMovementComponent();

	TArray<ATile*> getPossibleMovements(ATile* start, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FString playerTurn);

#pragma region Custom
private:
	TArray<ATile*> getAllPossibleMovementsChessman(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FString playerTurn);
	TArray<ATile*> getAllPossibleMovementsRook(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy);
	TArray<ATile*> getAllPossibleMovementsBishop(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy);
	TArray<ATile*> getAllPossibleMovementsKnight(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy);
	TArray<ATile*> getAllPossibleMovementsKing(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy);
	TArray<ATile*> getAllPossibleMovementsQueen(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy);
#pragma endregion


#pragma region Utils
public:
	TTuple<int, int> getTileIndex(ATile* tileTargetted, TArray<TArray<ATile*>> board);

private:
	TArray<ATile*> getPossibleMovementWithDirection(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FVector2D direction);
	bool checkIndexValidity(int line, int col, TArray<TArray<ATile*>> board);
#pragma endregion
};
