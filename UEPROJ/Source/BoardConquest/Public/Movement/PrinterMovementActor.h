// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Movement/IPrinterMovement.h"

#include "PrinterMovementActor.generated.h"

UCLASS()
class BOARDCONQUEST_API APrinterMovementActor : public AActor, public IPrinterMovement
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APrinterMovementActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma region DisplayOverline
public:
	virtual void highlightTile(ATile* tileToHighLight, bool value) override;
	virtual void highlightTroup(ATroup* troupToHighlight, bool value) override;
#pragma endregion
};
