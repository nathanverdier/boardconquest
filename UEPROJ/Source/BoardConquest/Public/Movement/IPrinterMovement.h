// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Troup.h"
#include "Map/Tile.h"

#include "IPrinterMovement.generated.h"
/**
 * 
 */
UINTERFACE(MinimalAPI, Blueprintable)
class UPrinterMovement : public UInterface
{
	GENERATED_BODY()

};

class BOARDCONQUEST_API IPrinterMovement
{
	GENERATED_BODY()

public:
	// Declare functions here
	virtual void highlightTile(ATile* tileToHighLight, bool value);
	virtual void highlightTroup(ATroup* troupToHighlight, bool value);
};