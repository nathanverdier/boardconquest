// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"

#include "Troup.h"
#include "StarPower.h"
#include "Troups/StaticTroup.h"
#include "Map/Tile.h"
#include "Movement/PrinterMovementActor.h"
#include "BoardConquestCamera.h"

#include "BoardConquestLevelScript.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ABoardConquestLevelScript : public ALevelScriptActor
{
	GENERATED_BODY()

public:
	ABoardConquestLevelScript();

protected:
	virtual void BeginPlay() override;
	
#pragma region BoardParametersAndCreatingFunctions
protected:
	UPROPERTY(EditAnywhere, Category = "Board Parameters")
		TSubclassOf<ATile> tileToSpawn;
	UPROPERTY(EditAnywhere, Category = "Board Parameters")
		float tileSize;

	TArray<TArray<ATile*>> board;

private:
	void createBoard();
#pragma endregion


#pragma region HandlePlayer
private:
	TArray<ATroup*> player1Army;
	TArray<ATroup*> player2Army;

private:
	void createAndSetUpAnArmy(FString player);
	void createAndSetUpCards(FString player);
#pragma endregion


#pragma region HandleCamera
protected:
	ABoardConquestCamera* cameraPlayer1;
	ABoardConquestCamera* camera2Player1;
	ABoardConquestCamera* camera3Player1;
	ABoardConquestCamera* cameraPlayer2;
	ABoardConquestCamera* camera2Player2;
	ABoardConquestCamera* camera3Player2;

	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer1")
		FVector initialCameraWorldPositionP1;
	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer1")
		FVector initialCamera2WorldPositionP1;
	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer1")
		FVector initialCamera3WorldPositionP1;

	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer2")
		FVector initialCameraWorldPositionP2;
	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer2")
		FVector initialCamera2WorldPositionP2;
	UPROPERTY(EditAnywhere, Category = "CameraPositionPlayer2")
		FVector initialCamera3WorldPositionP2;

	// Default Camera Rotations	Players 1
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer1")
		FRotator initialCameraWorldRotationP1;
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer1")
		FRotator initialCamera2WorldRotationP1;
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer1")
		FRotator initialCamera3WorldRotationP1;

	// Default Camera Rotations	Players 2
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer2")
		FRotator initialCameraWorldRotationP2;
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer2")
		FRotator initialCamera2WorldRotationP2;
	UPROPERTY(EditAnywhere, Category = "CameraRotationPlayer2")
		FRotator initialCamera3WorldRotationP2;

private:
	void spawnCamera();

public:
	ABoardConquestCamera* getCameraPlayer1();
	ABoardConquestCamera* getCamera2Player1();
	ABoardConquestCamera* getCamera3Player1();
	ABoardConquestCamera* getCameraPlayer2();
	ABoardConquestCamera* getCamera2Player2();
	ABoardConquestCamera* getCamera3Player2();
#pragma endregion
};
