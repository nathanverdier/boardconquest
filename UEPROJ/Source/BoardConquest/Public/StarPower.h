// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Troup.h"
#include "Map/Tile.h"
#include "Map/Tile.h"
#include "StarPowers/CardType.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystem.h"

#include "StarPower.generated.h"

UCLASS()
class BOARDCONQUEST_API AStarPower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStarPower();


#pragma region Usage
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Parameters")
		ECardType cardType;

public:
	UFUNCTION(BlueprintCallable)
		ECardType getCardType();

	virtual void usePower(AActor* target, FString actualPlayer);
	virtual TArray<AActor*> getAllPossibleTargettedActors(FString actualPlayer);
#pragma endregion


#pragma region ReloadingCard
protected:
	UPROPERTY(EditAnywhere, Category = "Reload Parameters")
		int turnNumberForAFullReload;
	UPROPERTY(EditAnywhere, Category = "Reload Parameters")
		int turnLeftBeforeReady;
	bool isReadyToUse;
	bool canBeUsedMoreThanOnce;

protected:
	void startReloadTime();
	void reloadTimeOver();

public:
	UFUNCTION(BlueprintCallable)
		bool isItReadyToUse();
	UFUNCTION(BlueprintCallable)
		int getTimeLeft();

	void reloadPower();
#pragma endregion


#pragma region ChangeActorTargettedStatus
protected:
	UPROPERTY(EditAnywhere, Category = "Actor Targetted Parameters")
		int statusDuration = 1;
	UPROPERTY(EditAnywhere, Category = "Actor Targetted Parameters")
		int turnLeftBeforeStatusReset;

protected:
	void startStatusDuration();
	virtual void endTargettedActorsStatus();

public:
	void updateTargettedActorsStatus();
#pragma endregion


#pragma region ParticleEffects
protected:
	UPROPERTY(EditAnywhere, Category = "Particles")
		class UParticleSystem* Particle;
	FString pathToParticle;
	FVector particleSize;

protected:
	void spawnParticleAtLocation(FVector targetLocation);
	void findAndSetParticle();
#pragma endregion
};
