// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "Troups/TroupRace.h"
#include "Troups/TroupRole.h"
#include "Templates/Tuple.h"

#include "Kismet/GameplayStatics.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

#include "Troup.generated.h"

UCLASS(ABSTRACT,Blueprintable)
class BOARDCONQUEST_API ATroup : public APawn
{
	GENERATED_BODY()


	
public:
	// Sets default values for this pawn's properties
	ATroup();

#pragma region InitializeFunctions
public:
	void initializeTroup(ETroupRace initialRace, ETroupRole initialRole, TTuple<int, int> initialBoardPosition, int initialAttackDamage, int initialDefence, int initialHealthPoint);
	void initializeTroup(ETroupRace initialRace, ETroupRole initialRole, int initialBoardLinePosition, int initialBoardColPosition, int initialAttackDamage, int initialDefence, int initialHealthPoint);
	void initializeTroup(ETroupRace initialRace, TTuple<int, int> initialBoardPosition);
	void initializeTroup(ETroupRace initialRace, int initialBoardLinePosition, int initialBoardColPosition);
#pragma endregion


#pragma region HandleCollision
protected:
	UPROPERTY(EditAnywhere, Category = "TreeComponents")
		class UBoxComponent* BoxCollision;

protected:
	virtual void setMeshComponent();

public:
	UBoxComponent* getBoxCollision();
	void initializeBoxCollision();
#pragma endregion

#pragma region Sound
protected:
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* knightTroupSelection;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* monsterTroupSelection;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* skeletonTroupSelection;

	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* knightTileSelection;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* monsterTileSelection;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* skeletonTileSelection;

	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* knightDeath;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* monsterDeath;
	UPROPERTY(EditAnywhere, Category = "Sound")
		class USoundBase* skeletonDeath;

public:
	void playTroupSelectionSound();
	void playTileSelectionSound();
	void playDeathSound();
#pragma endregion


#pragma region HandlePositionning
protected:
	UPROPERTY(VisibleAnywhere, Category = "Parameters")
		int lineBoardPosition;
	UPROPERTY(VisibleAnywhere, Category = "Parameters")
		int colBoardPosition;

public:
	UPROPERTY(VisibleAnywhere, Category = "Parameters")
		bool hasntMove = true;
	UPROPERTY(VisibleAnywhere, Category = "Parameters")
		bool isAllowedToMove = true;

public:
	TTuple<int, int> getBoardPosition();
	void getBoardPosition(int* line, int* col);
	void setBoardPosition(TTuple<int, int> newBoardPosition);
	void setBoardPosition(int line, int col);
#pragma endregion


#pragma region HandleStatistics
protected:
	UPROPERTY(EditAnywhere, Category = "Statistics")
		int attackDamage;
	UPROPERTY(EditAnywhere, Category = "Statistics")
		int defence;
	UPROPERTY(EditAnywhere, Category = "Statistics")
		int healthPoint;

public:
	int getDamage();
	int getDefence();
	int getLifePoint();

	void increaseDecreaseDamage(int value);
	void increaseDecreaseDefence(int value);
	void increaseDecreaseLife(int value);
#pragma endregion


#pragma region Others
protected:
	UPROPERTY(VisibleAnywhere, Category = "Parameters")
		ETroupRace troupRace;
	UPROPERTY(EditAnywhere, Category = "Parameters")
		ETroupRole troupRole;

public:
	UFUNCTION(BlueprintCallable)
		ETroupRace getTroupRace();
	UFUNCTION(BlueprintCallable)
		ETroupRole getTroupRole();
#pragma endregion


public:
	UFUNCTION(BlueprintImplementableEvent)
	void MoveTo(FVector location) ;
};
