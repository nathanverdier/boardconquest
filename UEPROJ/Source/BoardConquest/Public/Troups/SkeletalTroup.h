// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Troup.h"
#include "SkeletalTroup.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ASkeletalTroup : public ATroup
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASkeletalTroup();

#pragma region MeshComponent
protected:
	UPROPERTY(EditAnywhere, Category="Meshes")
		class USkeletalMeshComponent* SkeletalMeshComponent;
	UPROPERTY(EditAnywhere, Category = "Meshes")
		class USkeletalMesh* MeshKnight;
	UPROPERTY(EditAnywhere, Category = "Meshes")
		class USkeletalMesh* MeshMonster;
	UPROPERTY(EditAnywhere, Category = "Meshes")
		class USkeletalMesh* MeshSkeleton;

protected:
	virtual void setMeshComponent() override;

public:
	USkeletalMeshComponent* getSkeletalMesh();
#pragma endregion


#pragma region Animation
protected:
	UPROPERTY(EditAnywhere, Category = "Animations")
		TSubclassOf<UAnimInstance> KnightAnimation;
	UPROPERTY(EditAnywhere, Category = "Animations")
		TSubclassOf<UAnimInstance> MonsterAnimation;
	UPROPERTY(EditAnywhere, Category = "Animations")
		TSubclassOf<UAnimInstance> SkeletonAnimation;
#pragma endregion
};
