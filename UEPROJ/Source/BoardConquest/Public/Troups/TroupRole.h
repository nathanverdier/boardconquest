#pragma once


UENUM(BlueprintType)
enum class ETroupRole : uint8
{
    CHESSMAN = 0 UMETA(DisplayName = "CHESSMAN"),
    ROOK = 1 UMETA(DisplayName = "ROOK"),
    BISHOP = 2 UMETA(DisplayName = "BISHOP"),
    KNIGHT = 3 UMETA(DisplayName = "KNIGHT"),
    QUEEN = 4 UMETA(DisplayName = "QUEEN"),
    KING = 5 UMETA(DisplayName = "KING")
};