#pragma once


UENUM(BlueprintType)
enum class ETroupRace : uint8
{
    DEFAULT     = 0  UMETA(DisplayName = "DEFAULT"),
    KNIGHT      = 1  UMETA(DisplayName = "KNIGHT"),
    MONSTER     = 2  UMETA(DisplayName = "MONSTER"),
    SKELETON    = 3  UMETA(DisplayName = "SKELETON")
};