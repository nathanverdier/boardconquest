// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Troup.h"
#include "StaticTroup.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API AStaticTroup : public ATroup
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AStaticTroup();

#pragma region MeshComponent
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMesh* StaticMeshKnight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMesh* StaticMeshMonster;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMesh* StaticMeshSkeleton;

protected:
	virtual void setMeshComponent() override;

public:
	UStaticMeshComponent* getStaticMesh();
#pragma endregion
};