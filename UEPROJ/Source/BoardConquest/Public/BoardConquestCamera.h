// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "Kismet/GameplayStatics.h"
#include "BoardConquestCamera.generated.h"

/**
 * 
 */
UCLASS()
class BOARDCONQUEST_API ABoardConquestCamera : public ACameraActor
{
	GENERATED_BODY()
	
public:
	ABoardConquestCamera();

};
