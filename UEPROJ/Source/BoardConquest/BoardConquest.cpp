// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoardConquest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BoardConquest, "BoardConquest" );
