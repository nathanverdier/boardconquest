// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/RankLoss.h"
#include "BoardConquestGameInstance.h"
#include "../BoardConquestGameModeBase.h"

ARankLoss::ARankLoss()
    :AStarPower()
{
	cardType = ECardType::RANKLOSS;

    canBeUsedMoreThanOnce = false;
}

void ARankLoss::usePower(AActor* target, FString actualPlayer)
{
    ATroup* troupTargetted = Cast<ATroup>(target);
    if (!troupTargetted)
        return;

    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    ABoardConquestGameModeBase* tempGM = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!tempGM)
        return;
    

    if (troupTargetted->getTroupRole() == ETroupRole::KING
        || troupTargetted->getTroupRole() == ETroupRole::QUEEN
        || troupTargetted->getTroupRole() == ETroupRole::CHESSMAN)
        return;

    TSubclassOf<ATroup> BpToSpawn = NULL;
    for (auto tuple : tempGM->getAllBlueprintTroups())
    {
        if (tuple.Get<1>() == ETroupRole::CHESSMAN)
        {
            BpToSpawn = tuple.Get<0>();
            break;
        }
    }
    if (!BpToSpawn)
        return;

    int targetBoardLine = -1;
    int targetBoardCol = -1;
    troupTargetted->getBoardPosition(&targetBoardLine, &targetBoardCol);
    if (!(0 <= targetBoardLine && targetBoardLine < tempGI->board.Num())
        || !(0 <= targetBoardCol && targetBoardCol < tempGI->board[targetBoardLine].Num()))
        return;

    FTransform targetWorldTransfo = troupTargetted->GetTransform();
    ETroupRace targetRace = troupTargetted->getTroupRace();

    if (actualPlayer == "J1")
        tempGI->player2Army.Remove(troupTargetted);
    else if (actualPlayer == "J2")
        tempGI->player1Army.Remove(troupTargetted);
    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(nullptr);

    troupTargetted->Destroy();

    ATroup* myNewTroup = GetWorld()->SpawnActor<ATroup>(BpToSpawn, targetWorldTransfo);
    if (!myNewTroup)
        return;
    myNewTroup->initializeTroup(targetRace, targetBoardLine, targetBoardCol);
    
    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(myNewTroup);
    if (actualPlayer == "J1")
        tempGI->player2Army.Add(myNewTroup);
    else if (actualPlayer == "J2")
        tempGI->player1Army.Add(myNewTroup);

    Super::usePower(target, actualPlayer);
}

TArray<AActor*> ARankLoss::getAllPossibleTargettedActors(FString actualPlayer)
{
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    TArray<AActor*> armyPlayerAdverse;
    if (actualPlayer == "J1") {
        for (ATroup* myTroup : tempGI->player2Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING 
                && myTroup->getTroupRole() != ETroupRole::QUEEN 
                && myTroup->getTroupRole() != ETroupRole::CHESSMAN)
                armyPlayerAdverse.Add(myTroup);
        }
    }
    if (actualPlayer == "J2") {
        for (ATroup* myTroup : tempGI->player1Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING 
                && myTroup->getTroupRole() != ETroupRole::QUEEN 
                && myTroup->getTroupRole() != ETroupRole::CHESSMAN)
                armyPlayerAdverse.Add(myTroup);
        }
    }
    return armyPlayerAdverse;
}