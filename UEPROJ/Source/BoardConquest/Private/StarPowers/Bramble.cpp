// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/Bramble.h"

ABramble::ABramble()
    :AStarPower()
{
	cardType = ECardType::BRAMBLE;
    turnNumberForAFullReload = 10;
    statusDuration = 3;
}

#pragma region Power
void ABramble::usePower(AActor* target, FString actualPlayer) 
{
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    for (int i = 0; i < tempGI->board.Num(); i++)
    {
        for (int j = 1; j < (tempGI->board[i].Num() - 1); j++)
        {
            if (tempGI->board[i][j-1] && tempGI->board[i][j] && tempGI->board[i][j+1]) 
            {
                if (!tempGI->board[i][j - 1]->isOccupied() 
                    && !tempGI->board[i][j]->isOccupied() 
                    && !tempGI->board[i][j + 1]->isOccupied() 
                    && tempGI->board[i][j] == Cast<ATile>(target)) 
                {
                    myTilesOut.Add(tempGI->board[i][j-1]);
                    myTilesOut.Add(tempGI->board[i][j]);
                    myTilesOut.Add(tempGI->board[i][j+1]);

                    tempGI->board[i][j - 1] = nullptr;
                    tempGI->board[i][j] = nullptr;
                    tempGI->board[i][j + 1] = nullptr;

                    tileLigne = i;
                    tileColone = j - 1;
                }
            }
        }
    }

    for (ATile* outTile : myTilesOut)
    {
        outTile->spawnBramble();
    }

    if (myTilesOut.Num() > 0)
        Super::usePower(target, actualPlayer);
}

TArray<AActor*> ABramble::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> possibleTargets;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return possibleTargets;
    
    for (int i = 0; i < tempGI->board.Num(); i++)
    {
        for (int j = 1; j < (tempGI->board[i].Num()-1); j++)
        {
            if (tempGI->board[i][j - 1] && tempGI->board[i][j] && tempGI->board[i][j + 1])
            {
                if (!tempGI->board[i][j - 1]->isOccupied() && !tempGI->board[i][j]->isOccupied() && !tempGI->board[i][j + 1]->isOccupied())
                {
                    possibleTargets.Add(tempGI->board[i][j]);
                }
            }
        }
    }

    return possibleTargets;
}
#pragma endregion


#pragma region TargettedActorsStatus
void ABramble::endTargettedActorsStatus()
{
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    for (int i = 0; i < myTilesOut.Num(); i++) 
    {
        if (IsValid(myTilesOut[i]))
            tempGI->board[tileLigne][tileColone + i] = myTilesOut[i];
    }

    for (ATile* outTile : myTilesOut)
    {
        if (IsValid(outTile))
            outTile->destroyBramble();
    }
    myTilesOut.Empty();
}
#pragma endregion