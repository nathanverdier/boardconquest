// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/InstantClone.h"
#include "Components/BoxComponent.h"

#include "../BoardConquestGameModeBase.h"
#include "BoardConquest/Public/Management/BoardConquestManager.h"

AInstantClone::AInstantClone()
    :AStarPower()
{
    cardType = ECardType::INSTANTCLONE;
    turnNumberForAFullReload = 15;
}

void AInstantClone::usePower(AActor* target, FString actualPlayer)
{
    ATroup* troupTargetted = Cast<ATroup>(target);
    if (troupTargetted->getTroupRole() == ETroupRole::KING || troupTargetted->getTroupRole() == ETroupRole::QUEEN)
        return;

    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    bool found = false;
    ATroup* myNewTroup;
    int troupToSpawnLine;
    int troupToSpawnCol;
    TTuple<int, int> targetBoardPos = troupTargetted->getBoardPosition();

    found = findATileForDuplication(targetBoardPos.Get<0>(), targetBoardPos.Get<1>(), tempGI, actualPlayer, &troupToSpawnLine, &troupToSpawnCol);

    if (found)
    {
        myNewTroup = SpawnATroupOnTile(troupToSpawnLine, troupToSpawnCol, tempGI, troupTargetted);
        if (actualPlayer == "J1" && myNewTroup)
        {
            tempGI->player1Army.Add(myNewTroup);
        }
        else if (actualPlayer == "J2" && myNewTroup)
        {
            tempGI->player2Army.Add(myNewTroup);
        }
        Super::usePower(target, actualPlayer);
    }
}

TArray<AActor*> AInstantClone::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> possibleTargets;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return possibleTargets;

    int targetBoardLine = -1;
    int targetBoardCol = -1;
    
    if (actualPlayer == "J1") 
    {
        for (ATroup* myTroup : tempGI->player1Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
            {
                myTroup->getBoardPosition(&targetBoardLine, &targetBoardCol);
                if ((0 <= targetBoardLine && targetBoardLine < tempGI->board.Num())
                    && (0 <= targetBoardCol && targetBoardCol < tempGI->board[targetBoardLine].Num()))
                {
                    if (findATileForDuplication(targetBoardLine, targetBoardCol, tempGI, actualPlayer, &targetBoardLine, &targetBoardCol))
                        possibleTargets.Add(myTroup);
                }
            }
        }
    }
    else if (actualPlayer == "J2") 
    {
        for (ATroup* myTroup : tempGI->player2Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
            {
                myTroup->getBoardPosition(&targetBoardLine, &targetBoardCol);
                if ((0 <= targetBoardLine && targetBoardLine < tempGI->board.Num())
                    && (0 <= targetBoardCol && targetBoardCol < tempGI->board[targetBoardLine].Num()))
                {
                    if (findATileForDuplication(targetBoardLine, targetBoardCol, tempGI, actualPlayer, &targetBoardLine, &targetBoardCol))
                        possibleTargets.Add(myTroup);
                }
            }
        }
    }

    return possibleTargets;
}

bool AInstantClone::findATileForDuplication(const int line, const int col, UBoardConquestGameInstance* GI, FString player, int* validLine, int* validCol)
{
    int modifier = 1;

    if (player == "J2")
        modifier *= -1;

    if (line < GI->board.Num() && col < GI->board[line].Num())
    {
        if (0 <= line + modifier && line + modifier < GI->board.Num() 
            && 0 <= col && col < GI->board[line].Num()
            && !GI->board[line + modifier][col]->isOccupied())
        {
            *validLine = line + modifier;
            *validCol = col;
            return true;
        }
        else if (0 <= line && line < GI->board.Num() 
            && 0 <= col + modifier && col + modifier < GI->board[line].Num()
            && !GI->board[line][col + modifier]->isOccupied())
        {
            *validLine = line;
            *validCol = col + modifier;
            return true;
        }
        else if (0 <= line - modifier && line - modifier < GI->board.Num() 
            && 0 <= col && col < GI->board[line].Num() 
            && !GI->board[line - modifier][col]->isOccupied())
        {
            *validLine = line - modifier;
            *validCol = col;
            return true;
        }
        else if (0 <= line && line < GI->board.Num() 
            && 0 <= col - modifier && col - modifier < GI->board[line].Num() 
            && !GI->board[line][col - modifier]->isOccupied())
        {
            *validLine = line;
            *validCol = col - modifier;
            return true;
        }
        else
        {
            return false;
        }
    }

    return false;
}

ATroup* AInstantClone::SpawnATroupOnTile(const int line, const int col, UBoardConquestGameInstance* GI, ATroup* troupToDuplicate)
{
    TSubclassOf<ATroup> BpToSpawn = NULL;
    ABoardConquestGameModeBase* myGameMode = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
    ATroup* myNewTroup = nullptr;
    
    if (!myGameMode || !GI)
        return nullptr;

    for (auto tuple : myGameMode->getAllBlueprintTroups())
    {
        if (tuple.Get<1>() == troupToDuplicate->getTroupRole())
        {
            BpToSpawn = tuple.Get<0>();
            break;
        }
    }

    if (!BpToSpawn)
        return nullptr;

    if (line < GI->board.Num() && col < GI->board[line].Num())
    {
        FVector TroupToSpawnLocation = GI->board[line][col]->GetActorLocation();
        FRotator TroupToSpawnRotation = troupToDuplicate->GetActorRotation();
        TroupToSpawnLocation.Z = troupToDuplicate->GetActorLocation().Z;
        myNewTroup = GetWorld()->SpawnActor<ATroup>(BpToSpawn, TroupToSpawnLocation, TroupToSpawnRotation);
        if (myNewTroup)
        {
            myNewTroup->initializeTroup(troupToDuplicate->getTroupRace(), line, col);
            GI->board[line][col]->setOccupyingTroup(myNewTroup);
        }
    }

    return myNewTroup;
}