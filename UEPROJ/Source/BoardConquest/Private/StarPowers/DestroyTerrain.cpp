// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/DestroyTerrain.h"

ADestroyTerrain::ADestroyTerrain()
    :AStarPower()
{
    cardType = ECardType::DESTROYTERRAIN;

    canBeUsedMoreThanOnce = false;

    pathToParticle = "/Script/Engine.ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'";
    findAndSetParticle();
    particleSize = FVector(0.6, 0.6, 0.6);
}

#pragma region Power
void ADestroyTerrain::usePower(AActor* target, FString actualPlayer) 
{
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    for (int line = 0; line < tempGI->board.Num(); line++)
    {
        for (int col = 0; col < tempGI->board[line].Num(); col++)
        {
            if (tempGI->board[line][col] != nullptr) 
            {
                if (!tempGI->board[line][col]->isOccupied() && tempGI->board[line][col] == target) 
                {
                    spawnParticleAtLocation(tempGI->board[line][col]->GetActorLocation());
                    tempGI->board[line][col]->Destroy();
                    tempGI->board[line][col] = nullptr;
                    moveLeft--;
                }
            }
        }
    }

    if(moveLeft==0)
        Super::usePower(target, actualPlayer);
}

TArray<AActor*> ADestroyTerrain::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> availableTiles;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return availableTiles;

    for (int ligne = 0; ligne < tempGI->board.Num(); ligne++)
    {
        for (int i = 0; i < tempGI->board[ligne].Num(); i++)
        {
            if(tempGI->board[ligne][i] != nullptr)
                if (!tempGI->board[ligne][i]->isOccupied())
                    availableTiles.Add(tempGI->board[ligne][i]);
        }
    }
    return availableTiles;
}
#pragma endregion
