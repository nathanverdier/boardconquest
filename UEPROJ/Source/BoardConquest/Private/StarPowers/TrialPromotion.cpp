// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/TrialPromotion.h"

ATrialPromotion::ATrialPromotion()
    :AStarPower()
{
    cardType = ECardType::TRIALPROMOTION;

    turnNumberForAFullReload = 10;
    statusDuration = 0;
}

#pragma region Power
void ATrialPromotion::usePower(AActor* target, FString actualPlayer)
{
    ATroup* troupTargetted = Cast<ATroup>(target);
    if (!troupTargetted)
        return;
    troupTransformedOldRole = troupTargetted->getTroupRole();

    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    ABoardConquestGameModeBase* tempGM = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!tempGM)
        return;


    if (troupTargetted->getTroupRole() == ETroupRole::KING
        || troupTargetted->getTroupRole() == ETroupRole::QUEEN)
        return;

    TSubclassOf<ATroup> BpToSpawn = NULL;
    for (auto tuple : tempGM->getAllBlueprintTroups())
    {
        if (tuple.Get<1>() == ETroupRole::QUEEN)
        {
            BpToSpawn = tuple.Get<0>();
            break;
        }
    }
    if (!BpToSpawn)
        return;

    int targetBoardLine = -1;
    int targetBoardCol = -1;
    troupTargetted->getBoardPosition(&targetBoardLine, &targetBoardCol);
    if (!(0 <= targetBoardLine && targetBoardLine < tempGI->board.Num())
        || !(0 <= targetBoardCol && targetBoardCol < tempGI->board[targetBoardLine].Num()))
        return;

    FTransform targetWorldTransfo = troupTargetted->GetTransform();
    ETroupRace targetRace = troupTargetted->getTroupRace();

    if (actualPlayer == "J1")
        tempGI->player1Army.Remove(troupTargetted);
    else if (actualPlayer == "J2")
        tempGI->player2Army.Remove(troupTargetted);
    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(nullptr);

    troupTargetted->Destroy();

    ATroup* myNewTroup = GetWorld()->SpawnActor<ATroup>(BpToSpawn, targetWorldTransfo);
    if (!myNewTroup)
        return;
    myNewTroup->initializeTroup(targetRace, targetBoardLine, targetBoardCol);

    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(myNewTroup);
    if (actualPlayer == "J1")
        tempGI->player1Army.Add(myNewTroup);
    else if (actualPlayer == "J2")
        tempGI->player2Army.Add(myNewTroup);

    troupTransformed = myNewTroup;
    troupTransformedOwner = actualPlayer;

    Super::usePower(target, actualPlayer);
}

TArray<AActor*> ATrialPromotion::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> possibleTargets;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return possibleTargets;

    if (actualPlayer == "J1")
    {
        for (ATroup* myTroup : tempGI->player1Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }
    else if (actualPlayer == "J2")
    {
        for (ATroup* myTroup : tempGI->player2Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }

    return possibleTargets;
}
#pragma endregion


#pragma region TargettedActorsStatus
void ATrialPromotion::endTargettedActorsStatus()
{
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    ABoardConquestGameModeBase* tempGM = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!tempGM)
        return;

    TSubclassOf<ATroup> BpToSpawn = NULL;
    for (auto tuple : tempGM->getAllBlueprintTroups())
    {
        if (tuple.Get<1>() == troupTransformedOldRole)
        {
            BpToSpawn = tuple.Get<0>();
            break;
        }
    }
    if (!BpToSpawn)
        return;

    int targetBoardLine = -1;
    int targetBoardCol = -1;
    troupTransformed->getBoardPosition(&targetBoardLine, &targetBoardCol);
    if (!(0 <= targetBoardLine && targetBoardLine < tempGI->board.Num())
        || !(0 <= targetBoardCol && targetBoardCol < tempGI->board[targetBoardLine].Num()))
        return;

    FTransform targetWorldTransfo = troupTransformed->GetTransform();
    ETroupRace targetRace = troupTransformed->getTroupRace();

    if (troupTransformedOwner == "J1")
        tempGI->player1Army.Remove(troupTransformed);
    else if (troupTransformedOwner == "J2")
        tempGI->player2Army.Remove(troupTransformed);
    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(nullptr);

    troupTransformed->Destroy();

    ATroup* myNewTroup = GetWorld()->SpawnActor<ATroup>(BpToSpawn, targetWorldTransfo);
    if (!myNewTroup)
        return;
    myNewTroup->initializeTroup(targetRace, targetBoardLine, targetBoardCol);

    tempGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(myNewTroup);
    if (troupTransformedOwner == "J1")
        tempGI->player1Army.Add(myNewTroup);
    else if (troupTransformedOwner == "J2")
        tempGI->player2Army.Add(myNewTroup);

    troupTransformed = nullptr;
}
#pragma endregion