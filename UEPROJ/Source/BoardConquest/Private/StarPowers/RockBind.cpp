// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/RockBind.h"

ARockBind::ARockBind()
    :AStarPower()
{
    cardType = ECardType::ROCKBIND;

    turnNumberForAFullReload = 10;
    statusDuration = 2;
}

#pragma region Power
void ARockBind::usePower(AActor* target, FString actualPlayer) 
{
    ATroup* myPawn = Cast<ATroup>(target);

    if (!myPawn)
        return;

    if (myPawn->getTroupRole() == ETroupRole::KING || myPawn->getTroupRole() == ETroupRole::QUEEN)
        return;

    myPawn->isAllowedToMove = false;

    troupTargetted = myPawn;

    Super::usePower(target, actualPlayer);
}

TArray<AActor*> ARockBind::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> possibleTargets;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return possibleTargets;

    if (actualPlayer == "J1")
    {
        for (ATroup* myTroup : tempGI->player2Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }
    else if (actualPlayer == "J2")
    {
        for (ATroup* myTroup : tempGI->player1Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }

    return possibleTargets;
}
#pragma endregion


#pragma region TargettedActorsStatus
void ARockBind::endTargettedActorsStatus()
{
    if (IsValid(troupTargetted))
        troupTargetted->isAllowedToMove = true;
}
#pragma endregion