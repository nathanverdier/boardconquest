// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPowers/InstantDeath.h"
#include "Components/BoxComponent.h"

AInstantDeath::AInstantDeath()
    :AStarPower()
{
    cardType = ECardType::INSTANTDEATH;

    canBeUsedMoreThanOnce = false;
}

void AInstantDeath::usePower(AActor* target, FString actualPlayer)
{
    ATroup *troupTargetted = Cast<ATroup>(target);
    if (!troupTargetted)
        return;

    if (troupTargetted->getTroupRole() == ETroupRole::KING || troupTargetted->getTroupRole() == ETroupRole::QUEEN)
        return;

    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return;

    int troupTargettedLine, troupTargettedCol;
    troupTargetted->getBoardPosition(&troupTargettedLine, &troupTargettedCol);

    if (!(0 <= troupTargettedLine && troupTargettedLine < tempGI->board.Num())
        || !(0 <= troupTargettedCol && troupTargettedCol < tempGI->board[troupTargettedLine].Num()))
        return;

    tempGI->board[troupTargettedLine][troupTargettedCol]->setOccupyingTroup(nullptr);

    if (actualPlayer == "J1")
        tempGI->player1Army.Remove(troupTargetted);
    else if (actualPlayer == "J2")
        tempGI->player2Army.Remove(troupTargetted);

    troupTargetted->playDeathSound();
    troupTargetted->Destroy();

    Super::usePower(target, actualPlayer);
}

TArray<AActor*> AInstantDeath::getAllPossibleTargettedActors(FString actualPlayer)
{
    TArray<AActor*> possibleTargets;
    UBoardConquestGameInstance* tempGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
    if (!tempGI)
        return possibleTargets;

    if (actualPlayer == "J1") {
        for (ATroup* myTroup : tempGI->player2Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }
    if (actualPlayer == "J2") {
        for (ATroup * myTroup : tempGI->player1Army)
        {
            if (myTroup->getTroupRole() != ETroupRole::KING && myTroup->getTroupRole() != ETroupRole::QUEEN)
                possibleTargets.Add(myTroup);
        }
    }
    return possibleTargets;
}