// Fill out your copyright notice in the Description page of Project Settings.


#include "Map/InitMap.h"
#include "Map/TileType.h"
#include <BoardConquest/Public/BoardConquestGameInstance.h>


#pragma region Initializing
void AInitMap::initializeMapCreatorComponent(UWorld* newWorld, EBoardType myNewBoardType, EBoardStyle myNewBoardStyle, TSubclassOf<ATile> myNewTileToSpawn, float myNewTileSize)
{
	// Set up world reference
	world = newWorld;

	// Set up board parameters
	boardType = myNewBoardType;
	boardStyle = myNewBoardStyle;
	tileToSpawn = myNewTileToSpawn;
	tileSize = myNewTileSize;

	// Say that initialize function has been called
	mapInitializeFunctionHasBeenCalled = true;
}

void AInitMap::initializeArmyCreatorComponent(UWorld* newWorld, EBoardType myNewBoardType, TArray<TTuple<TSubclassOf<ATroup>, ETroupRole>> troups)
{
	// Set up world reference
	world = newWorld;

	// Set up board parameters
	boardType = myNewBoardType;

	// Set up army parameters
	for (auto tuple : troups)
	{
		switch (tuple.Get<1>())
		{
		case ETroupRole::CHESSMAN:
			chessmanToSpawn = tuple.Get<0>();
			break;
		case ETroupRole::ROOK:
			rookToSpawn = tuple.Get<0>();
			break;
		case ETroupRole::BISHOP:
			bishopToSpawn = tuple.Get<0>();
			break;
		case ETroupRole::KNIGHT:
			knightToSpawn = tuple.Get<0>();
			break;
		case ETroupRole::QUEEN:
			queenToSpawn = tuple.Get<0>();
			break;
		case ETroupRole::KING:
			kingToSpawn = tuple.Get<0>();
			break;
		default:
			UE_LOG(LogTemp, Error, TEXT("InitMap has received an unknown ETroupRole"));
			break;
		}
	}

	// Say that initialize function has been called
	armyInitializeFunctionHasBeenCalled = true;
}

void AInitMap::initializeCardsCreatorComponent() {

	// Set up world reference
	world = GetWorld();

	// Say that initialize function has been called
	cardsInitializeFunctionHasBeenCalled = true;
}
#pragma endregion


#pragma region CreatingAndSpawningBoard
TArray<TArray<ATile*>> AInitMap::createAndSpawnBoard()
{
	UE_LOG(LogTemp, Warning, TEXT("Creating board........"));
	TArray<TArray<ATile*>> tiles;

	if (!mapInitializeFunctionHasBeenCalled)
	{
		UE_LOG(LogTemp, Error, TEXT("MapCreator component has not been initialize"));
		return tiles;
	}

	switch (boardType)
	{
	case EBoardType::DEFAULT:
		boardLength = 8;
		boardWidth = 8;
		break;
	case EBoardType::CLASSIC:
		boardLength = 8;
		boardWidth = 8;
		break;
	case EBoardType::DOUBLE:
		boardLength = 8;
		boardWidth = 15;
		break;
	case EBoardType::ONEONLY:
		boardLength = 8;
		boardWidth = 5;
		break;
	default:
		break;
	}

	FVector firstSpawnLocation = FVector(-boardWidth / 2 * tileSize, -boardLength / 2 * tileSize, 0);
	if (boardWidth % 2 == 0)
		firstSpawnLocation.X += tileSize / 2;
	if (boardLength % 2 == 0)
		firstSpawnLocation.Y += tileSize / 2;
	FVector nextSpawnLocation = firstSpawnLocation;

	for (int column = 0; column < boardLength; column++)
	{
		TArray<ATile*> myLineTiles;
		for (int line = 0; line < boardWidth; line++)
		{
			myLineTiles.Add(spawnTile(nextSpawnLocation));
			nextSpawnLocation.X += tileSize;
		}
		tiles.Add(myLineTiles);
		nextSpawnLocation.X = firstSpawnLocation.X;
		nextSpawnLocation.Y += tileSize;
	}

	setColorToBoard(tiles);

	return tiles;
}

ATile* AInitMap::spawnTile(FVector spawnLocation)
{
	ATile* newTile = NULL;
	UE_LOG(LogTemp, Warning, TEXT("Spawning ATile at %s"), *spawnLocation.ToString());
	if (tileToSpawn)
		newTile = world->SpawnActor<ATile>(tileToSpawn, spawnLocation, FRotator(0.f));
	else
		UE_LOG(LogTemp, Warning, TEXT("ATile has tried to spawn but blueprint is not set"));

	if (newTile)
		newTile->worldLocation = spawnLocation;

	return newTile;
}

void AInitMap::setColorToBoard(TArray<TArray<ATile*>> board)
{
	int typeRank = 0;

	TArray<ETileType> types;
	switch (boardStyle)
	{
	case EBoardStyle::DEFAULT:
		types.Add(ETileType::WHITE);
		types.Add(ETileType::BLACK);
		break;
	case EBoardStyle::CLASSIC:
		types.Add(ETileType::WHITE);
		types.Add(ETileType::BLACK);
		break;
	case EBoardStyle::BROWN:
		types.Add(ETileType::LIGHTBROWN);
		types.Add(ETileType::DEEPBROWN);
		break;
	default:
		types.Add(ETileType::BLACK);
		types.Add(ETileType::WHITE);
		break;
	}
	

	for (TArray<ATile*> line : board)
	{
		for (ATile* tile : line)
		{
			tile->setType(types[typeRank % types.Num()]);
			typeRank++;
		}

		if (boardWidth % 2 == 0)
			typeRank++;
	}
}
#pragma endregion


#pragma region CreatingAndSpawningTroups
TArray<TTuple<TTuple<int, int>, ETroupRole>> AInitMap::armyComposition(FString player)
{
	switch (boardType)
	{
	case EBoardType::DEFAULT:
		return armyCompositionClassic(player);
		break;
	case EBoardType::CLASSIC:
		return armyCompositionClassic(player);
		break;
	case EBoardType::DOUBLE:
		return armyCompositionDouble(player);
		break;
	case EBoardType::ONEONLY:
		return armyCompositionOneOnly(player);
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("UnitMap received an unknown EBoardType"));
		return TArray<TTuple<TTuple<int, int>, ETroupRole>>();
		break;
	}
}

TArray<TTuple<TTuple<int, int>, ETroupRole>> AInitMap::armyCompositionOneOnly(FString player)
{
	TArray<TTuple<TTuple<int, int>, ETroupRole>> comp;

	if (player == "J1")
	{
		comp.Add(MakeTuple(MakeTuple(0, 0), ETroupRole::ROOK));
		comp.Add(MakeTuple(MakeTuple(0, 1), ETroupRole::BISHOP));
		comp.Add(MakeTuple(MakeTuple(0, 2), ETroupRole::KING));
		comp.Add(MakeTuple(MakeTuple(0, 3), ETroupRole::QUEEN));
		comp.Add(MakeTuple(MakeTuple(0, 4), ETroupRole::KNIGHT));
	}
	else if (player == "J2")
	{
		comp.Add(MakeTuple(MakeTuple(7, 4), ETroupRole::ROOK));
		comp.Add(MakeTuple(MakeTuple(7, 3), ETroupRole::BISHOP));
		comp.Add(MakeTuple(MakeTuple(7, 2), ETroupRole::KING));
		comp.Add(MakeTuple(MakeTuple(7, 1), ETroupRole::QUEEN));
		comp.Add(MakeTuple(MakeTuple(7, 0), ETroupRole::KNIGHT));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UnitMap received an unknown player string"));
		return comp;
	}

	int line;
	if (player == "J1")
		line = 1;
	else
		line = 6;

	for (int col = 0; col < 5; col++)
		comp.Add(MakeTuple(MakeTuple(line, col), ETroupRole::CHESSMAN));

	return comp;
}

TArray<TTuple<TTuple<int, int>, ETroupRole>> AInitMap::armyCompositionClassic(FString player)
{
	TArray<TTuple<TTuple<int, int>, ETroupRole>> comp;
	int line;

	if (player == "J1")
		line = 0;
	else if (player == "J2")
		line = 7;
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UnitMap received an unknown player string"));
		return comp;
	}

	comp.Add(MakeTuple(MakeTuple(line, 0), ETroupRole::ROOK));
	comp.Add(MakeTuple(MakeTuple(line, 1), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 2), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 3), ETroupRole::KING));
	comp.Add(MakeTuple(MakeTuple(line, 4), ETroupRole::QUEEN));
	comp.Add(MakeTuple(MakeTuple(line, 5), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 6), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 7), ETroupRole::ROOK));

	if (player == "J1")
		line = 1;
	else
		line = 6;

	for (int col = 0; col < 8; col++)
		comp.Add(MakeTuple(MakeTuple(line, col), ETroupRole::CHESSMAN));

	return comp;
}

TArray<TTuple<TTuple<int, int>, ETroupRole>> AInitMap::armyCompositionDouble(FString player)
{
	TArray<TTuple<TTuple<int, int>, ETroupRole>> comp;
	int line;

	if (player == "J1")
		line = 0;
	else if (player == "J2")
		line = 7;
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UnitMap received an unknown player string"));
		return comp;
	}

	comp.Add(MakeTuple(MakeTuple(line, 0), ETroupRole::ROOK));
	comp.Add(MakeTuple(MakeTuple(line, 1), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 2), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 3), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 4), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 5), ETroupRole::ROOK));
	comp.Add(MakeTuple(MakeTuple(line, 6), ETroupRole::QUEEN));
	comp.Add(MakeTuple(MakeTuple(line, 7), ETroupRole::KING));
	comp.Add(MakeTuple(MakeTuple(line, 8), ETroupRole::QUEEN));
	comp.Add(MakeTuple(MakeTuple(line, 9), ETroupRole::ROOK));
	comp.Add(MakeTuple(MakeTuple(line, 10), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 11), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 12), ETroupRole::BISHOP));
	comp.Add(MakeTuple(MakeTuple(line, 13), ETroupRole::KNIGHT));
	comp.Add(MakeTuple(MakeTuple(line, 14), ETroupRole::ROOK));

	if (player == "J1")
		line = 1;
	else
		line = 6;

	for (int col = 0; col < 15; col++)
		comp.Add(MakeTuple(MakeTuple(line, col), ETroupRole::CHESSMAN));

	return comp;
}

TArray<ATroup*> AInitMap::spawnTroupsOnBoard(TArray<TTuple<TTuple<int, int>, ETroupRole>> composition, TArray<TArray<ATile*>> board, ETroupRace troupRace)
{
	TArray<ATroup*> army;
	ATroup* temp_ptr;

	for (auto tuple : composition)
	{
		if (tuple.Get<0>().Get<0>() >= 0 && tuple.Get<0>().Get<0>() < board.Num()
			&& tuple.Get<0>().Get<1>() >= 0 && tuple.Get<0>().Get<1>() < board[0].Num())
		{
			temp_ptr = spawnTroupOnTile(tuple.Get<1>(), board[tuple.Get<0>().Get<0>()][tuple.Get<0>().Get<1>()]);
			if (temp_ptr)
			{
				army.Add(temp_ptr);
				temp_ptr->initializeTroup(troupRace, tuple.Get<0>());
				board[tuple.Get<0>().Get<0>()][tuple.Get<0>().Get<1>()]->setOccupyingTroup(temp_ptr);

				switch (troupRace)
				{
				case ETroupRace::DEFAULT:
					break;
				case ETroupRace::KNIGHT:
					break;
				case ETroupRace::MONSTER:
					break;
				case ETroupRace::SKELETON:
					temp_ptr->AddActorWorldRotation(FRotator(0, 90, 0));
					break;
				default:
					break;
				}
			}
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("ArmyCreator component didn't spawn a troup because the board hasn't got the right size"));
	}

	return army;
}

ATroup* AInitMap::spawnTroupOnTile(ETroupRole troupRole, ATile* tile)
{
	ATroup* temp_ptr = nullptr;
	int troupHeight = 10;

	switch (troupRole)
	{
	case ETroupRole::CHESSMAN:
		if (chessmanToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(chessmanToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	case ETroupRole::ROOK:
		if (rookToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(rookToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	case ETroupRole::BISHOP:
		if (bishopToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(bishopToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	case ETroupRole::KNIGHT:
		if (knightToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(knightToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	case ETroupRole::QUEEN:
		if (queenToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(queenToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	case ETroupRole::KING:
		if (kingToSpawn && world && tile)
			temp_ptr = world->SpawnActor<ATroup>(kingToSpawn, tile->worldLocation + FVector(0, 0, troupHeight), FRotator(0.f));
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("UnitMap::spawnTroupOnTile received an unknown ETroupRole"));
		break;
	}

	return temp_ptr;
}

TArray<ATroup*> AInitMap::createAndSpawnPlayerArmy(FString player, ETroupRace troupRace, TArray<TArray<ATile*>> board)
{
	TArray<ATroup*> army;
	if (!armyInitializeFunctionHasBeenCalled)
	{
		UE_LOG(LogTemp, Error, TEXT("ArmyCreator component has not been initialized"));
		return army;
	}

	TArray<TTuple<TTuple<int, int>, ETroupRole>> armyComp = armyComposition(player);

	army = spawnTroupsOnBoard(armyComp, board, troupRace);

	if (player == "J2")
	{
		for (auto troup : army)
			troup->AddActorWorldRotation(FRotator(0, -180, 0));
	}

	return army;
}
#pragma endregion 

#pragma region CreatingAndSpawningCards
AStarPower* AInitMap::spawnACard(ECardType cardType, FVector worldLocation)
{
	AStarPower* card = nullptr;

	switch (cardType)
	{
	case ECardType::INSTANTDEATH:
		card = world->SpawnActor<AInstantDeath>(AInstantDeath::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::INSTANTCLONE:
		card = world->SpawnActor<AInstantClone>(AInstantClone::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::BRAMBLE:
		card = world->SpawnActor<ABramble>(ABramble::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::DESTROYTERRAIN:
		card = world->SpawnActor<ADestroyTerrain>(ADestroyTerrain::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::ROCKBIND:
		card = world->SpawnActor<ARockBind>(ARockBind::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::RANKLOSS:
		card = world->SpawnActor<ARankLoss>(ARankLoss::StaticClass(), worldLocation, FRotator(0));
		break;
	case ECardType::TRIALPROMOTION:
		card = world->SpawnActor<ATrialPromotion>(ATrialPromotion::StaticClass(), worldLocation, FRotator(0));
		break;
	default:
		break;
	}

	return card;
}

TArray<AStarPower*> AInitMap::createAndSpawnPlayerCards(FString player)
{
	TArray<AStarPower*> playerHand;

	world = GetWorld();
	UBoardConquestGameInstance* myGI = nullptr;

	if (world)
		myGI = Cast<UBoardConquestGameInstance>(world->GetGameInstance());

	if (myGI)
	{
		AStarPower* starpower;
		if (player == "J1")
		{
			for (auto cardType : myGI->Player1HandTypes) 
			{
				starpower = spawnACard(cardType, FVector(0, -500, 0));
				if (starpower)
					playerHand.Add(starpower);
			}
		}
		else if (player == "J2")
		{
			for (auto cardType : myGI->Player2HandTypes)
			{
				starpower = spawnACard(cardType, FVector(0, 500, 0));
				if (starpower)
					playerHand.Add(starpower);
			}
		}
	}

	return playerHand;
}
#pragma endregion 
