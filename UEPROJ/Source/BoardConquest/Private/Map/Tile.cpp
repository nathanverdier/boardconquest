// Fill out your copyright notice in the Description page of Project Settings.


#include "Map/Tile.h"
#include "Components/BoxComponent.h"
#include "UObject/Class.h"

// Sets default values
ATile::ATile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	setUpTreeComponents();

	type = ETileType::WHITE;
	worldLocation = FVector(0, 0, 0);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
	setMaterial();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

#pragma region TreeComponents
void ATile::setUpTreeComponents()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	RootComponent = BoxCollision;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(BoxCollision);
}

UStaticMeshComponent* ATile::getStaticMesh()
{
	return StaticMesh;
}
#pragma endregion

#pragma region CustomMaterial
void ATile::setMaterial()
{
	switch (type)
	{
	case ETileType::WHITE:
		setWhiteMaterial();
		break;
	case ETileType::BLACK:
		setBlackMaterial();
		break;
	case ETileType::LIGHTBROWN:
		setLightBrownMaterial();
		break;
	case ETileType::DEEPBROWN:
		setDeepBrownMaterial();
		break;
	default:
		setDefaultMaterial();
		break;
	}
}

void ATile::setWhiteMaterial()
{
	if (whiteMaterial)
		StaticMesh->SetMaterial(0, whiteMaterial);
	else
		UE_LOG(LogTemp, Warning, TEXT("whiteMaterial is not set"));
}

void ATile::setBlackMaterial()
{
	if (blackMaterial)
		StaticMesh->SetMaterial(0, blackMaterial);
	else
		UE_LOG(LogTemp, Warning, TEXT("blackMaterial is not set"));
}

void ATile::setLightBrownMaterial()
{
	if (lightBrownMaterial)
		StaticMesh->SetMaterial(0, lightBrownMaterial);
	else
		UE_LOG(LogTemp, Warning, TEXT("lightBrownMaterial is not set"));
}

void ATile::setDeepBrownMaterial()
{
	if (deepBrownMaterial)
		StaticMesh->SetMaterial(0, deepBrownMaterial);
	else
		UE_LOG(LogTemp, Warning, TEXT("deepBrownMaterial is not set"));
}

void ATile::setDefaultMaterial()
{
	if (defaultMaterial)
		StaticMesh->SetMaterial(0, defaultMaterial);
	else
		UE_LOG(LogTemp, Warning, TEXT("defaultMaterial is not set"));
}

void ATile::setType(ETileType newTileType)
{
	type = newTileType;
	UE_LOG(LogTemp, Warning, TEXT("type of a tile has been set to %s"), *UEnum::GetValueAsString(type));
	setMaterial();
}
#pragma endregion


#pragma region HandleOccupyingTroup
ATroup* ATile::getOccupyingTroup()
{
	return occupyingTroup;
}

bool ATile::isOccupied()
{
	if (occupyingTroup == nullptr && isAvailable)
		return false;
	else
		return true;
}

void ATile::setOccupyingTroup(ATroup* newOccupyingTroup)
{
	occupyingTroup = newOccupyingTroup;
}
#pragma endregion


#pragma region Spawnables
void ATile::spawnBramble()
{
	destroyBramble();

	if (BrambleToSpawnClass)
	{
		FVector pos = GetActorLocation();
		pos.Z += -20;
		activeBramble = GetWorld()->SpawnActor<AActor>(BrambleToSpawnClass, pos, FRotator(0));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATile::spawnBramble() : BrambleToSpawnClass is not set in blueprint"));
	}
}

void ATile::destroyBramble()
{
	if (activeBramble)
		activeBramble->Destroy();
}
#pragma endregion