// Fill out your copyright notice in the Description page of Project Settings.


#include "BoardConquestGameInstance.h"

UBoardConquestGameInstance::UBoardConquestGameInstance()
{
}

void UBoardConquestGameInstance::menuSetting(FString GamerTagJ1, FString GamerTagJ2, ETroupRace Joueur1RacePawn, ETroupRace Joueur2RacePawn, EBoardStyle newTileColor, EBoardType newDimensionBoard)
{
	Joueur1GamerTag = GamerTagJ1;
	Joueur2GamerTag = GamerTagJ2;
	Joueur1Race = Joueur1RacePawn;
	Joueur2Race = Joueur2RacePawn;
	myTileColor = newTileColor;
	myDimensionBoard = newDimensionBoard;
}


void UBoardConquestGameInstance::resetGameInstance()
{
	Joueur1GamerTag = "";
	Joueur2GamerTag = "";
	Joueur1Race = ETroupRace::KNIGHT;
	Joueur2Race = ETroupRace::KNIGHT;
	myTileColor = EBoardStyle::CLASSIC;
	myDimensionBoard = EBoardType::CLASSIC;
	board.Empty();
	Player1HandTypes.Empty();
	Player2HandTypes.Empty();
	player1Army.Empty();
	player2Army.Empty();
	player1Hand.Empty();
	player2Hand.Empty();
	actuRandom = true;
}


EBoardType UBoardConquestGameInstance::getBoardDimension()
{
	return myDimensionBoard;
}