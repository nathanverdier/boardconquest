// Fill out your copyright notice in the Description page of Project Settings.


#include "Management/BoardConquestManager.h"
#include "BoardConquestLevelScript.h"
#include "BoardConquest/BoardConquestGameModeBase.h"

// Sets default values
ABoardConquestManager::ABoardConquestManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Set selected actors to nullptr
	selectedTile = nullptr;
	selectedTroup = nullptr;
	selectedStarPower = nullptr;
}

// Called when the game starts or when spawned
void ABoardConquestManager::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialize displayer
	initializeDisplayer();

	// Initialize firstplayercontroller left click
	UWorld* World = GetWorld();
	if (World)
	{
		firstPlayerController = World->GetFirstPlayerController();
		firstPlayerController->InputComponent->BindAction("OnLeftClick", IE_Pressed, this, &ABoardConquestManager::onLeftClick); 
		cameraManager = GetWorld()->SpawnActor<ABoardConquestCameraManager>(ABoardConquestCameraManager::StaticClass(), FVector(0), FRotator(0));
	}

	// Set up game instance
	setUpGameInstance();

	// Create troup movement component
	if (World)
	{
		possibleMovementHandler = World->SpawnActor<ATroupMovementComponent>(ATroupMovementComponent::StaticClass(), FVector(0), FRotator(0));
	}

	// player Turn
	playerTurn = "J1";

	// Initialise player view
	initializePlayerView();
}

// Called every frame
void ABoardConquestManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABoardConquestManager::setUpGameInstance()
{
	UWorld* world = GetWorld();
	UGameInstance* tempGI = nullptr;

	if (world)
		tempGI = world->GetGameInstance();

	if (tempGI)
		myGI = Cast<UBoardConquestGameInstance>(tempGI);
}

#pragma region HandleSelectedActors
void ABoardConquestManager::setSelectedTile(ATile* myNewSelectedTile)
{
	if (selectedTroup != nullptr)
		selectedTile = myNewSelectedTile;
}

void ABoardConquestManager::setSelectedTroup(ATroup* myNewSelectedTroup)
{
	if (selectedTroup == myNewSelectedTroup)
		return;

	if (selectedTroup == nullptr)
	{
		selectedTroup = myNewSelectedTroup;
		outlineActor(selectedTroup, true);
	}
	else
	{
		outlineActor(selectedTroup, false);
		for (ATile* tile : currentPossibleMovement)
		{
			if (IsValid(tile))
				outlineActor(tile, false);
		}

		selectedTroup = myNewSelectedTroup;
		outlineActor(selectedTroup, true);
	}

	selectedTroup->playTroupSelectionSound();

	int line;
	int col;
	selectedTroup->getBoardPosition(&line, &col);

	if (playerTurn == "J1")
		currentPossibleMovement = possibleMovementHandler->getPossibleMovements(myGI->board[line][col], myGI->board, myGI->player1Army, playerTurn);
	else if (playerTurn == "J2")
		currentPossibleMovement = possibleMovementHandler->getPossibleMovements(myGI->board[line][col], myGI->board, myGI->player2Army, playerTurn);
	
	for (ATile* tile : currentPossibleMovement)
		outlineActor(tile, true);
}

void ABoardConquestManager::onLeftClick()
{
	UE_LOG(LogTemp, Warning, TEXT("ABoardConquestManager::onLeftClick() : LeftClick pressed"));
	FHitResult hitResult;
	if (firstPlayerController->GetHitResultUnderCursor(ECC_Visibility, true, hitResult))
	{
		auto actorClicked = hitResult.GetActor();
		if (selectedStarPower)
		{
			if (checkIfStarPowerCanBeUsedOn(actorClicked))
			{
				useActiveStarPowerOn(actorClicked);
			}
		}
		else if (ATile* tileClicked = Cast<ATile>(actorClicked))
		{
			UE_LOG(LogTemp, Warning, TEXT("ABoardConquestManager::onLeftClick() : Tile clicked"));
			if (selectedTroup && movementAllowed(tileClicked))
			{
				setSelectedTile(tileClicked);
				playTurn();
			}
		}
		else if (ATroup* troupClicked = Cast<ATroup>(actorClicked))
		{
			UE_LOG(LogTemp, Warning, TEXT("ABoardConquestManager::onLeftClick() : Troup clicked"));
			if (playerTurn == "J1" && !myGI->player1Army.Contains(troupClicked))
				return;
			else if (playerTurn == "J2" && !myGI->player2Army.Contains(troupClicked))
				return;

			setSelectedTroup(troupClicked);
		}
		else 
		{
			if (selectedTroup)
			{
				outlineActor(selectedTroup, false);
				selectedTroup = nullptr;

				for (ATile* tile : currentPossibleMovement)
				{
					if (IsValid(tile))
						outlineActor(tile, false);
				}
				currentPossibleMovement.Empty();
			}
		}
	}
}

ATroup* ABoardConquestManager::getSelectedTroup()
{
	return selectedTroup;
}
#pragma endregion


#pragma region Cards
bool ABoardConquestManager::checkIfStarPowerCanBeUsedOn(AActor* actorTargetted)
{
	if (selectedStarPower)
	{
		return possibleActorsToUseStarPowerOn.Contains(actorTargetted);
	}
	else
	{
		return false;
	}
}

void ABoardConquestManager::useActiveStarPowerOn(AActor* actorTargetted)
{
	selectedStarPower->usePower(actorTargetted, playerTurn);
	if (IsValid(actorTargetted))
		outlineActor(actorTargetted, false);
	possibleActorsToUseStarPowerOn.Remove(actorTargetted);
	cardCancelled();
}

bool ABoardConquestManager::isACardActive() 
{
	bool value;
	if (selectedStarPower)
		value = true;
	else
		value = false;
	return value;
}

void ABoardConquestManager::cardClicked(AStarPower* cardReferencedByUIClicked)
{
	bool cardHasBeenFound = false;
	bool cardIsReady = true;

	if (selectedTroup)
	{
		outlineActor(selectedTroup, false);
		selectedTroup = nullptr;
		for (auto tile : currentPossibleMovement)
			outlineActor(tile, false);
		currentPossibleMovement.Empty();
	}

	if (playerTurn == "J1")
	{
		for (auto currentCard : myGI->player1Hand)
		{
			if (currentCard == cardReferencedByUIClicked)
			{
				if (currentCard->isItReadyToUse())
				{
					selectedStarPower = currentCard;
					cardHasBeenFound = true;
				}
				else
				{
					cardIsReady = false;
				}
				break;
			}
		}
	}
	else if (playerTurn == "J2")
	{
		for (auto currentCard : myGI->player2Hand)
		{
			if (currentCard == cardReferencedByUIClicked)
			{
				if (currentCard->isItReadyToUse())
				{
					selectedStarPower = currentCard;
					cardHasBeenFound = true;
				}
				else
				{
					cardIsReady = false;
				}
				break;
			}
		}
	}
	else
	{

	}

	if (!cardHasBeenFound)
	{
		UE_LOG(LogTemp, Warning, TEXT("Unknown card clicked"));
	}
	else
	{
		if (cardIsReady)
		{
			UE_LOG(LogTemp, Warning, TEXT("A known card has been clicked"));
			possibleActorsToUseStarPowerOn = selectedStarPower->getAllPossibleTargettedActors(playerTurn);
			for (auto actor : possibleActorsToUseStarPowerOn)
				outlineActor(actor, true);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("A known card has been clicked but is not ready to use"));
		}
	}
}

void ABoardConquestManager::cardCancelled()
{
	UE_LOG(LogTemp, Warning, TEXT("CardClicked has been cancelled"));
	for (auto actor : possibleActorsToUseStarPowerOn)
	{
		if (IsValid(actor))
			outlineActor(actor, false);
	}
	possibleActorsToUseStarPowerOn.Empty();
	selectedStarPower = nullptr;
}

bool ABoardConquestManager::checkIfACardIsReadyToUse(ECardType cardTypeTaregetted, FString Player)
{
	if (Player == "J1")
	{
		for (auto currentCard : myGI->player1Hand)
		{
			if (currentCard->getCardType() == cardTypeTaregetted)
			{
				return currentCard->isItReadyToUse();
			}
		}
	}
	else if (Player == "J2")
	{
		for (auto currentCard : myGI->player2Hand)
		{
			if (currentCard->getCardType() == cardTypeTaregetted)
			{
				return currentCard->isItReadyToUse();
			}
		}
	}
	return false;
}
#pragma endregion


#pragma region DisplayingOutlines
void ABoardConquestManager::outlineActor(AActor* actorToOutline, bool value)
{
	if (displayerInitialized && IsValid(actorToOutline) && IsValid(myDisplayer))
	{
		if (ATroup* troup = Cast<ATroup>(actorToOutline))
			myDisplayer->highlightTroup(troup, value);
		else if (ATile* tile = Cast<ATile>(actorToOutline))
			myDisplayer->highlightTile(tile, value);
		else
			UE_LOG(LogTemp, Warning, TEXT("An Actor has tried to be outlined but it is not tile or troup"));
	}
}

void ABoardConquestManager::initializeDisplayer()
{
	myDisplayer = GetWorld()->SpawnActor<APrinterMovementActor>(APrinterMovementActor::StaticClass(), FVector(0, 0, -100), FRotator(0));
	if (myDisplayer != nullptr)
		displayerInitialized = true;
}
#pragma endregion


#pragma region HandleMovement
void ABoardConquestManager::moveTroupTo(ATroup* troupToMove, ATile* tileTargetted)
{
	FVector troupOldPos;
	FVector tileTargettedLocation;
	int troupOldBoardPosX, troupOldBoardPosY;

	troupToMove->getBoardPosition(&troupOldBoardPosX, &troupOldBoardPosY);
	troupOldPos = troupToMove->GetActorLocation();

	tileTargettedLocation = tileTargetted->GetActorLocation();

	if (tileTargetted->isOccupied())
	{
		if (tileTargetted->getOccupyingTroup()->getTroupRole() == ETroupRole::KING)
			gameIsOver = true;

		if (playerTurn == "J1")
			myGI->player2Army.Remove(tileTargetted->getOccupyingTroup());
		else if (playerTurn == "J2")
			myGI->player1Army.Remove(tileTargetted->getOccupyingTroup());

		tileTargetted->getOccupyingTroup()->playDeathSound();
		tileTargetted->getOccupyingTroup()->Destroy();
		tileTargetted->setOccupyingTroup(nullptr);
	}
	
	tileTargetted->setOccupyingTroup(troupToMove);
	for (int i = 0; i < myGI->board.Num(); i++)
	{
		for (int j = 0; j < myGI->board[i].Num(); j++)
		{
			if (tileTargetted == myGI->board[i][j])
			{
				troupToMove->setBoardPosition(i, j);
			}
		}
	}
	myGI->board[troupOldBoardPosX][troupOldBoardPosY]->setOccupyingTroup(nullptr);
	troupToMove->hasntMove = false;

	if (troupToMove->getTroupRole() == ETroupRole::CHESSMAN)
	{
		int value = 1;
		if (playerTurn == "J2")
			value = -1;

		if (troupToMove->getBoardPosition().Get<0>() + value < 0 || troupToMove->getBoardPosition().Get<0>() + value >= myGI->board.Num())
		{
			transformChessmanInQueen(&troupToMove);
			selectedTroup = troupToMove;
		}
	}

	troupToMove->playTileSelectionSound();
	troupToMove->MoveTo(FVector(tileTargettedLocation.X, tileTargettedLocation.Y, troupOldPos.Z));
}

bool ABoardConquestManager::movementAllowed(ATile* tileTargetted)
{
	bool result = false;

	if (currentPossibleMovement.Contains(tileTargetted))
		result = true;

	return result;
}

void ABoardConquestManager::transformChessmanInQueen(ATroup** troupToTransform)
{
	ABoardConquestGameModeBase* tempGM = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!tempGM)
		return;

	TSubclassOf<ATroup> BpToSpawn = NULL;
	for (auto tuple : tempGM->getAllBlueprintTroups())
	{
		if (tuple.Get<1>() == ETroupRole::QUEEN)
		{
			BpToSpawn = tuple.Get<0>();
			break;
		}
	}
	if (!BpToSpawn)
		return;

	int targetBoardLine = -1;
	int targetBoardCol = -1;
	(*troupToTransform)->getBoardPosition(&targetBoardLine, &targetBoardCol);
	if (!(0 <= targetBoardLine && targetBoardLine < myGI->board.Num())
		|| !(0 <= targetBoardCol && targetBoardCol < myGI->board[targetBoardLine].Num()))
		return;

	FTransform targetWorldTransfo = (*troupToTransform)->GetTransform();
	ETroupRace targetRace = (*troupToTransform)->getTroupRace();

	if (playerTurn == "J1")
		myGI->player1Army.Remove((*troupToTransform));
	else if (playerTurn == "J2")
		myGI->player2Army.Remove((*troupToTransform));
	myGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(nullptr);

	(*troupToTransform)->Destroy();

	ATroup* myNewTroup = GetWorld()->SpawnActor<ATroup>(BpToSpawn, targetWorldTransfo);
	if (!myNewTroup)
		return;
	myNewTroup->initializeTroup(targetRace, targetBoardLine, targetBoardCol);

	myGI->board[targetBoardLine][targetBoardCol]->setOccupyingTroup(myNewTroup);
	if (playerTurn == "J1")
		myGI->player1Army.Add(myNewTroup);
	else if (playerTurn == "J2")
		myGI->player2Army.Add(myNewTroup);

	(*troupToTransform) = myNewTroup;
}
#pragma endregion


#pragma region HandlePlayer
void ABoardConquestManager::playTurn()
{
	if (!selectedTile && !selectedTroup)
		return;

	moveTroupTo(selectedTroup, selectedTile);

	outlineActor(selectedTroup, false);
	selectedTile = nullptr;
	selectedTroup = nullptr;
	for (ATile* tile : currentPossibleMovement)
		outlineActor(tile, false);
	currentPossibleMovement.Empty();

	if (!gameIsOver)
	{
		if (playerTurn == "J1")
		{
			playerTurn = "J2";
			for (auto card : myGI->player1Hand)
				card->reloadPower();
			for (auto card : myGI->player2Hand)
				card->updateTargettedActorsStatus();
		}
		else if (playerTurn == "J2")
		{
			playerTurn = "J1";
			for (auto card : myGI->player2Hand)
				card->reloadPower();
			for (auto card : myGI->player1Hand)
				card->updateTargettedActorsStatus();
		}

		switchPlayerView();
	}
}

FString ABoardConquestManager::getPlayerTurn()
{
	return playerTurn;
}

FString ABoardConquestManager::getPlayerNameTurn()
{
	FString playerTurnGamerTag;

	if (playerTurn == "J1")
		playerTurnGamerTag = myGI->Joueur1GamerTag;
	else if (playerTurn == "J2")
		playerTurnGamerTag = myGI->Joueur2GamerTag;

	return playerTurnGamerTag;
}
#pragma endregion


#pragma region HandleCamera
void ABoardConquestManager::initializePlayerView()
{
	auto level = GetWorld()->GetLevelScriptActor();
	if (level)
		if (auto boardConquestLevelScript = Cast<ABoardConquestLevelScript>(level)) {
			cameraPlayer1 = boardConquestLevelScript->getCameraPlayer1();
			camera2Player1 = boardConquestLevelScript->getCamera2Player1();
			camera3Player1 = boardConquestLevelScript->getCamera3Player1();
			cameraPlayer2 = boardConquestLevelScript->getCameraPlayer2();
			camera2Player2 = boardConquestLevelScript->getCamera2Player2();
			camera3Player2 = boardConquestLevelScript->getCamera3Player2();
		}
	if (firstPlayerController && cameraPlayer1 && cameraPlayer2)
		firstPlayerController->SetViewTarget(cameraPlayer1);
}
#pragma endregion

#pragma region SetViewCurrentUser
void ABoardConquestManager::switchPlayerView()
{
	if (playerTurn == "J1")
		cameraManager->switchView(cameraPlayer1, 3.f);
	else
		cameraManager->switchView(cameraPlayer2, 3.f);
}

void ABoardConquestManager::setViewCam2() {
	if (playerTurn == "J1")
		cameraManager->switchView(camera2Player1, 1.5f);
	else
		cameraManager->switchView(camera2Player2, 1.5f);
}

void ABoardConquestManager::setViewCam3() {

	if (playerTurn == "J1")
		cameraManager->switchView(camera3Player1, 2.f);
	else
		cameraManager->switchView(camera3Player2, 2.f);
}

#pragma endregion

#pragma region End
bool ABoardConquestManager::isThegameOver()
{
	return gameIsOver;
}
#pragma endregion