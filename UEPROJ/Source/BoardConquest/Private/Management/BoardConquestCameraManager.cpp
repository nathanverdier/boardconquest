// Fill out your copyright notice in the Description page of Project Settings.


#include "Management/BoardConquestCameraManager.h"

// Sets default values
ABoardConquestCameraManager::ABoardConquestCameraManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ABoardConquestCameraManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABoardConquestCameraManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

#pragma region SwitchView
void ABoardConquestCameraManager::switchView(ABoardConquestCamera* camera,float time)
{
	//Find the actor that handles control for the local player.
	APlayerController* OurPlayerController = GetWorld()->GetFirstPlayerController();

	//Smoothly transition to our actor on begin play.
	OurPlayerController->SetViewTargetWithBlend(camera, time);
}
#pragma endregion
