// Fill out your copyright notice in the Description page of Project Settings.


#include "BoardConquestLevelScript.h"
#include "BoardConquestGameInstance.h"
#include "Map/InitMap.h"
#include "../BoardConquestGameModeBase.h"
#include "Management/BoardConquestManager.h"


ABoardConquestLevelScript::ABoardConquestLevelScript()
{
	// Default tile size value
	tileSize = 100;

	// Default Camera Positions Players 1
	initialCameraWorldPositionP1 = FVector(0, -650, 800);
	initialCamera2WorldPositionP1 = FVector(0, -250, 850);
	initialCamera3WorldPositionP1 = FVector(830, -620, 135);

	// Default Camera Positions Players 2
	initialCameraWorldPositionP2 = FVector(0, 650, 800);
	initialCamera2WorldPositionP2 = FVector(0, 250, 850);
	initialCamera3WorldPositionP2 = FVector(-830, 620, 700);
	
	// Default Camera Rotations Players 1 
	initialCameraWorldRotationP1 = FRotator(0, -60, 90);
	initialCamera2WorldRotationP1 = FRotator(0, -80, 90);
	initialCamera3WorldRotationP1 = FRotator(-20, -40, 135);

	// Default Camera Rotations	Players 2
	initialCameraWorldRotationP2 = FRotator(0, -60, 90);
	initialCamera2WorldRotationP2 = FRotator(0, -80, 90);
	initialCamera3WorldRotationP2 = FRotator(-20, -45, -45);
}

void ABoardConquestLevelScript::BeginPlay()
{
	Super::BeginPlay();

	// Create board
	createBoard();
	
	// Create armies
	createAndSetUpAnArmy("J1");
	createAndSetUpAnArmy("J2");

	// Spawn Camera
	spawnCamera();

	// Spawn Cards
	createAndSetUpCards("J1");
	createAndSetUpCards("J2");

	// Spawn Manager
	ABoardConquestManager* manager = GetWorld()->SpawnActor<ABoardConquestManager>(ABoardConquestManager::StaticClass(), FVector(0, 0, 200), FRotator(0));
}


#pragma region Map
void ABoardConquestLevelScript::createBoard()
{
	AInitMap* mapCreator = GetWorld()->SpawnActor<AInitMap>(AInitMap::StaticClass(), FVector(0), FRotator(0));

	UBoardConquestGameInstance* myGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
		mapCreator->initializeMapCreatorComponent(GetWorld(), myGI->myDimensionBoard, myGI->myTileColor, tileToSpawn, tileSize);
	else
		mapCreator->initializeMapCreatorComponent(GetWorld(), EBoardType::ONEONLY, EBoardStyle::CLASSIC, tileToSpawn, tileSize);

	board = mapCreator->createAndSpawnBoard();

	myGI->board = board;

	mapCreator->Destroy();
}
#pragma endregion


#pragma region CreateEntities
void ABoardConquestLevelScript::createAndSetUpAnArmy(FString player)
{
	AInitMap* troupCreator = GetWorld()->SpawnActor<AInitMap>(AInitMap::StaticClass(), FVector(0), FRotator(0));

	UBoardConquestGameInstance* myGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
	ABoardConquestGameModeBase* myGM = Cast<ABoardConquestGameModeBase>(GetWorld()->GetAuthGameMode());
	if (myGI && myGM)
		troupCreator->initializeArmyCreatorComponent(GetWorld(), myGI->myDimensionBoard, myGM->getAllBlueprintTroups());
	else
	{
		UE_LOG(LogTemp, Error, TEXT("GameMode is not ABoardConquestGameModeBase"));
		return;
	}
	
	if (player == "J1")
		myGI->player1Army = troupCreator->createAndSpawnPlayerArmy(player, myGI->Joueur1Race, board);
	else if (player == "J2")
		myGI->player2Army = troupCreator->createAndSpawnPlayerArmy(player, myGI->Joueur2Race, board);

	troupCreator->Destroy();
}

void ABoardConquestLevelScript::createAndSetUpCards(FString player)
{
	AInitMap* cardsCreator = GetWorld()->SpawnActor<AInitMap>(AInitMap::StaticClass(), FVector(0), FRotator(0));

	UBoardConquestGameInstance* myGI = Cast<UBoardConquestGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
		cardsCreator->initializeCardsCreatorComponent();
	else
	{
		UE_LOG(LogTemp, Error, TEXT("GameInstance is not ABoardConquestGameInstance"));
		return;
	}

	if (player == "J1")
		myGI->player1Hand = cardsCreator->createAndSpawnPlayerCards(player);
	else if (player == "J2")
		myGI->player2Hand = cardsCreator->createAndSpawnPlayerCards(player);

	cardsCreator->Destroy();
}
#pragma endregion


#pragma region HandleCamera
void ABoardConquestLevelScript::spawnCamera()
{
	cameraPlayer1 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCameraWorldPositionP1, initialCameraWorldRotationP1);
	cameraPlayer2 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCameraWorldPositionP2, initialCameraWorldRotationP2);
	camera2Player1 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCamera2WorldPositionP1, initialCamera2WorldRotationP1);
	camera3Player1 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCamera3WorldPositionP1, initialCamera3WorldRotationP1);
	camera2Player2 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCamera2WorldPositionP2, initialCamera2WorldRotationP2);
	camera3Player2 = GetWorld()->SpawnActor<ABoardConquestCamera>(ABoardConquestCamera::StaticClass(), initialCamera3WorldPositionP2, initialCamera3WorldRotationP2);
}

ABoardConquestCamera* ABoardConquestLevelScript::getCameraPlayer1()
{
	return cameraPlayer1;
}

ABoardConquestCamera* ABoardConquestLevelScript::getCamera2Player1()
{
	return camera2Player1;
}

ABoardConquestCamera* ABoardConquestLevelScript::getCamera3Player1()
{
	return camera3Player1;
}

ABoardConquestCamera* ABoardConquestLevelScript::getCameraPlayer2()
{
	return cameraPlayer2;
}

ABoardConquestCamera* ABoardConquestLevelScript::getCamera2Player2()
{
	return camera2Player2;
}

ABoardConquestCamera* ABoardConquestLevelScript::getCamera3Player2()
{
	return camera3Player2;
}
#pragma endregion