// Fill out your copyright notice in the Description page of Project Settings.


#include "StarPower.h"
#include "Components/BoxComponent.h"

// Sets default values
AStarPower::AStarPower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	isReadyToUse = true;
	canBeUsedMoreThanOnce = true;

	turnNumberForAFullReload = 1;

	statusDuration = -1;
	turnLeftBeforeStatusReset = -1;
}

#pragma region Usage
void AStarPower::usePower(AActor* target, FString actualPlayer) 
{
	startReloadTime();
	startStatusDuration();
}

TArray<AActor*> AStarPower::getAllPossibleTargettedActors(FString actualPlayer) { return TArray<AActor*>(); }

ECardType AStarPower::getCardType()
{
	return cardType;
}
#pragma endregion


#pragma region ReloadingPower
bool AStarPower::isItReadyToUse()
{
	return isReadyToUse;
}

int AStarPower::getTimeLeft()
{
	return turnLeftBeforeReady;
}

void AStarPower::startReloadTime()
{
	isReadyToUse = false;
	turnLeftBeforeReady = turnNumberForAFullReload;
}

void AStarPower::reloadTimeOver()
{
	isReadyToUse = true;
}

void AStarPower::reloadPower()
{
	if (canBeUsedMoreThanOnce)
	{
		if (turnLeftBeforeReady > 0)
			turnLeftBeforeReady--;
		else
			reloadTimeOver();
	}
}
#pragma endregion


#pragma region ChangeActorTargettedStatus
void AStarPower::startStatusDuration()
{
	turnLeftBeforeStatusReset = statusDuration;
}

void AStarPower::endTargettedActorsStatus() {}

void AStarPower::updateTargettedActorsStatus()
{
	if (turnLeftBeforeStatusReset == 0)
		endTargettedActorsStatus();
	
	if (turnLeftBeforeStatusReset >= 0)
		turnLeftBeforeStatusReset -= 1;
}
#pragma endregion


#pragma region ParticleEffects
void AStarPower::spawnParticleAtLocation(FVector targetLocation)
{
	if (Particle != NULL)
	{
		// Transform --> Location, Rotation, Scale
		FTransform ParticleT;
		// Spawn Location
		ParticleT.SetLocation(targetLocation);
		// World Size Particule
		ParticleT.SetScale3D(particleSize);
		// Spawn Particule
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, ParticleT, true);
	}
}

void AStarPower::findAndSetParticle()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ObjectFinder(*pathToParticle);
	if (ObjectFinder.Succeeded())
	{
		Particle = ObjectFinder.Object;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Erreur lors du chargement de la particule."));
	}
}
#pragma endregion