// Fill out your copyright notice in the Description page of Project Settings.


#include "Movement/PrinterMovementActor.h"
#include "Troups/SkeletalTroup.h"
#include "Troups/StaticTroup.h"

// Sets default values
APrinterMovementActor::APrinterMovementActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void APrinterMovementActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APrinterMovementActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APrinterMovementActor::highlightTile(ATile* tileToHighLight, bool value)
{
	tileToHighLight->getStaticMesh()->SetRenderCustomDepth(value);
}

void APrinterMovementActor::highlightTroup(ATroup* troupToHighlight, bool value)
{
	if (auto skeletalTroup = Cast<ASkeletalTroup>(troupToHighlight))
	{
		skeletalTroup->getSkeletalMesh()->SetRenderCustomDepth(value);
	}
	else if (auto staticTroup = Cast<AStaticTroup>(troupToHighlight))
	{
		staticTroup->getStaticMesh()->SetRenderCustomDepth(value);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("APrinterMovementActor::highlightTroup IS NOT IMPLEMENTED ON THIS ACTOR YET"));
	}
}