// Fill out your copyright notice in the Description page of Project Settings.


#include "Movement/TroupMovementComponent.h"

ATroupMovementComponent::ATroupMovementComponent()
{
}

ATroupMovementComponent::~ATroupMovementComponent()
{
}

TArray<ATile*> ATroupMovementComponent::getPossibleMovements(ATile* start, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FString playerTurn)
{
	TArray<ATile*> result = TArray<ATile*>();
	TTuple<int, int> indexTile = getTileIndex(start, board);
	
	if (!start || !start->isOccupied())
		return result;

	if (!start->getOccupyingTroup()->isAllowedToMove)
		return result;

	switch (start->getOccupyingTroup()->getTroupRole())
	{
	case ETroupRole::CHESSMAN :
		result = getAllPossibleMovementsChessman(start, indexTile,board, allyArmy, playerTurn);
		break;
	case ETroupRole::ROOK:
		result = getAllPossibleMovementsRook(start, indexTile, board, allyArmy);
		break;
	case ETroupRole::BISHOP:
		result = getAllPossibleMovementsBishop(start, indexTile, board, allyArmy);
		break;
	case ETroupRole::KNIGHT:
		result = getAllPossibleMovementsKnight(start, indexTile, board, allyArmy);
		break;
	case ETroupRole::KING:
		result = getAllPossibleMovementsKing(start, indexTile, board, allyArmy);
		break;
	case ETroupRole::QUEEN:
		result = getAllPossibleMovementsQueen(start, indexTile, board, allyArmy);
		break;
	default:
		break;
	}


	return result;
}

#pragma region Custom
TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsChessman(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FString playerTurn)
{
	TArray<ATile*> result = TArray<ATile*>();
	int line = tileIndex.Get<0>();
	int col = tileIndex.Get<1>();

	int turnModifier = 1;
	if (!(playerTurn == "J1"))
		turnModifier = -1;

	if (checkIndexValidity(line + turnModifier, col, board))
	{
		if (!(board[line + turnModifier][col]->isOccupied()))
		{
			result.Add(board[line + turnModifier][col]);
			if (start->getOccupyingTroup()->hasntMove)
			{
				if (checkIndexValidity(line + turnModifier * 2, col, board))
				{
					if (!(board[line + turnModifier * 2][col]->isOccupied()))
						result.Add(board[line + turnModifier * 2][col]);
				}
			}
		}
	}

	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			if (!(i == 0) && !(j == 0))
			{
				if (checkIndexValidity(line + i, col + j, board))
				{
					if (board[line + i][col + j]->isOccupied())
					{
						if (!allyArmy.Contains(board[line + i][col + j]->getOccupyingTroup()))
							result.Add(board[line + i][col + j]);
					}
				}
			}
		}
	}

	return result;
}

TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsRook(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy)
{
	TArray<ATile*> result = TArray<ATile*>();
	TArray<ATile*> resultTemp;

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(1, 0));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(-1, 0));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(0, 1));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(0, -1));
	for (auto tile : resultTemp)
		result.Add(tile);

	return result;
}

TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsBishop(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy)
{
	TArray<ATile*> result = TArray<ATile*>();
	TArray<ATile*> resultTemp;

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(1, 1));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(-1, -1));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(-1, 1));
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getPossibleMovementWithDirection(start, tileIndex, board, allyArmy, FVector2D(1, -1));
	for (auto tile : resultTemp)
		result.Add(tile);

	return result;
}

TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsKnight(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy)
{
	TArray<ATile*> result = TArray<ATile*>();
	int line = tileIndex.Get<0>();
	int col = tileIndex.Get<1>();

	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			if (!(i == 0) && !(j == 0))
			{
				if (checkIndexValidity(line + i * 2, col + j, board))
				{
					if (!board[line + i * 2][col + j]->isOccupied())
						result.Add(board[line + i * 2][col + j]);
					else
					{
						if (!allyArmy.Contains(board[line + i * 2][col + j]->getOccupyingTroup()))
							result.Add(board[line + i * 2][col + j]);
					}
				}
				if (checkIndexValidity(line + i, col + j * 2, board))
				{
					if (!board[line + i][col + j * 2]->isOccupied())
						result.Add(board[line + i][col + j * 2]);
					else
					{
						if (!allyArmy.Contains(board[line + i][col + j * 2]->getOccupyingTroup()))
							result.Add(board[line + i][col + j * 2]);
					}
				}
			}
		}
	}

	return result;
}

TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsKing(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy)
{
	TArray<ATile*> result = TArray<ATile*>();
	int line = tileIndex.Get<0>();
	int col = tileIndex.Get<1>();

	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			if (!(i == 0 && j == 0))
			{
				if (checkIndexValidity(line + i, col + j, board))
				{
					if (!board[line + i][col + j]->isOccupied())
						result.Add(board[line + i][col + j]);
					else
					{
						if (!allyArmy.Contains(board[line + i][col + j]->getOccupyingTroup()))
							result.Add(board[line + i][col + j]);
					}
				}
			}
		}
	}

	return result;
}

TArray<ATile*> ATroupMovementComponent::getAllPossibleMovementsQueen(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy)
{
	TArray<ATile*> result = TArray<ATile*>();
	TArray<ATile*> resultTemp;

	resultTemp = getAllPossibleMovementsBishop(start, tileIndex, board, allyArmy);
	for (auto tile : resultTemp)
		result.Add(tile);

	resultTemp = getAllPossibleMovementsRook(start, tileIndex, board, allyArmy);
	for (auto tile : resultTemp)
		result.Add(tile);

	return result;
}
#pragma endregion


#pragma region Utils
TTuple<int, int> ATroupMovementComponent::getTileIndex(ATile* tileTargetted, TArray<TArray<ATile*>> board)
{
	int lineI = 0;
	int colI = 0;
	for (auto line : board)
	{
		for (auto currentTile : line)
		{
			if (tileTargetted == currentTile)
				return TTuple<int, int>(lineI, colI);
			colI++;
		}
		lineI++;
		colI = 0;
	}
	return TTuple<int, int>();
}

TArray<ATile*> ATroupMovementComponent::getPossibleMovementWithDirection(ATile* start, TTuple<int, int> tileIndex, TArray<TArray<ATile*>> board, TArray<ATroup*> allyArmy, FVector2D direction)
{
	TArray<ATile*> result = TArray<ATile*>();
	ATile* currentTile = start;
	int line = tileIndex.Get<0>();
	int col = tileIndex.Get<1>();

	if (!checkIndexValidity(line, col, board))
		return result;
	
	line += direction.X;
	col += direction.Y;
	while (checkIndexValidity(line, col, board))
	{
		currentTile = board[line][col];
		if (currentTile->isOccupied())
		{
			if (!allyArmy.Contains(currentTile->getOccupyingTroup()))
				result.Add(currentTile);
			break;
		}
		result.Add(currentTile);
		line += direction.X;
		col += direction.Y;
	}

	return result;
}

bool ATroupMovementComponent::checkIndexValidity(int line, int col, TArray<TArray<ATile*>> board)
{
	return (line >= 0 && line < board.Num() && col >= 0 && col < board[line].Num() && board[line][col]);
}
#pragma endregion