// Fill out your copyright notice in the Description page of Project Settings.


#include "Troups/SkeletalTroup.h"
#include "Components/BoxComponent.h"

ASkeletalTroup::ASkeletalTroup()
    :ATroup()
{
    SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
    UBoxComponent* MyBoxCollision = getBoxCollision();
    static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshDefault(TEXT("/Script/Engine.SkeletalMesh'/Game/CustomContent/Mesh/Default/SkeletonMesh/P1Test/character.character'"));

    if (MeshDefault.Succeeded()) {
        SkeletalMeshComponent->SetSkeletalMesh(MeshDefault.Object);
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("Erreur lors du chargement du mesh."));
    }

    if (MyBoxCollision)
    {
        SkeletalMeshComponent->SetupAttachment(MyBoxCollision);
    }
}


#pragma region MeshComponent
void ASkeletalTroup::setMeshComponent()
{
    switch (troupRace) 
    {
    case ETroupRace::KNIGHT:
        if (MeshKnight && KnightAnimation)
        {
            SkeletalMeshComponent->SetSkeletalMesh(MeshKnight);
            SkeletalMeshComponent->SetAnimInstanceClass(KnightAnimation);
        }
        break;
    case ETroupRace::MONSTER:
        if (MeshMonster && MonsterAnimation)
        {
            SkeletalMeshComponent->SetSkeletalMesh(MeshMonster);
            SkeletalMeshComponent->SetAnimInstanceClass(MonsterAnimation);
        }
        break;
    case ETroupRace::SKELETON:
        if (MeshSkeleton && SkeletonAnimation)
        {
            SkeletalMeshComponent->SetSkeletalMesh(MeshSkeleton);
            SkeletalMeshComponent->SetAnimInstanceClass(SkeletonAnimation);
        }
        break;
    }
}

USkeletalMeshComponent* ASkeletalTroup::getSkeletalMesh() { return SkeletalMeshComponent; }
#pragma endregion