// Fill out your copyright notice in the Description page of Project Settings.

#include "Troups/StaticTroup.h"
#include "Components/BoxComponent.h"

AStaticTroup::AStaticTroup()
    :ATroup()
{
    StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    UBoxComponent* MyBoxCollision = getBoxCollision();
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshDefault(TEXT("/Script/Engine.StaticMesh'/Game/CustomContent/Mesh/Default/StaticMesh/SM_Statue.SM_Statue'"));

    if (MeshDefault.Succeeded()) {
        StaticMesh->SetStaticMesh(MeshDefault.Object);
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("Erreur lors du chargement du mesh."));
    }

    if (MyBoxCollision)
    {
        StaticMesh->SetupAttachment(MyBoxCollision);
    }
}


#pragma region MeshComponent
void AStaticTroup::setMeshComponent()
{
    switch (troupRace) {
    case ETroupRace::KNIGHT:
        if (StaticMeshKnight)
            StaticMesh->SetStaticMesh(StaticMeshKnight);
        break;
    case ETroupRace::MONSTER:
        if (StaticMeshMonster)
            StaticMesh->SetStaticMesh(StaticMeshMonster);
        break;
    case ETroupRace::SKELETON:
        if (StaticMeshSkeleton)
            StaticMesh->SetStaticMesh(StaticMeshSkeleton);
        break;
    }
}

UStaticMeshComponent* AStaticTroup::getStaticMesh() { return StaticMesh; }

#pragma endregion