// Fill out your copyright notice in the Description page of Project Settings.


#include "Troup.h"
#include "Components/BoxComponent.h"

// Sets default values
ATroup::ATroup()
{
	PrimaryActorTick.bCanEverTick = false;

	// Create BoxComponent and set as RootComponent for the Actor
	initializeBoxCollision();

	// Setting Up default parameters
	troupRace = ETroupRace::DEFAULT;
	troupRole = ETroupRole::CHESSMAN;
	attackDamage = 1;
	defence = 1;
	healthPoint = 1;
}

void ATroup::setMeshComponent()
{
}

#pragma region InitializeFunctions
void ATroup::initializeTroup(ETroupRace initialRace, ETroupRole initialRole, TTuple<int, int> initialBoardPosition, int initialAttackDamage, int initialDefence, int initialHealthPoint)
{
	initializeTroup(initialRace, initialRole, initialBoardPosition.Get<0>(), initialBoardPosition.Get<1>(), initialAttackDamage, initialDefence, initialHealthPoint);
	
}

void ATroup::initializeTroup(ETroupRace initialRace, ETroupRole initialRole, int initialBoardLinePosition, int initialBoardColPosition, int initialAttackDamage, int initialDefence, int initialHealthPoint)
{
	troupRace = initialRace;
	troupRole = initialRole;
	lineBoardPosition = initialBoardLinePosition;
	colBoardPosition = initialBoardColPosition;
	attackDamage = initialAttackDamage;
	defence = initialDefence;
	healthPoint = initialHealthPoint;
}

void ATroup::initializeTroup(ETroupRace initialRace, TTuple<int, int> initialBoardPosition)
{
	initializeTroup(initialRace, initialBoardPosition.Get<0>(), initialBoardPosition.Get<1>());
}

void ATroup::initializeTroup(ETroupRace initialRace, int initialBoardLinePosition, int initialBoardColPosition)
{
	troupRace = initialRace;
	lineBoardPosition = initialBoardLinePosition;
	colBoardPosition = initialBoardColPosition;

	setMeshComponent();
}
#pragma endregion


#pragma region HandleCollision
UBoxComponent* ATroup::getBoxCollision() { return BoxCollision; }

void ATroup::initializeBoxCollision()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	if (BoxCollision)
		RootComponent = BoxCollision;
	else
		UE_LOG(LogTemp, Warning, TEXT("ATroup : Failed to initialize BoxCollision"));
}
#pragma endregion


#pragma region HandlePositionning
TTuple<int, int> ATroup::getBoardPosition() { return MakeTuple(lineBoardPosition, colBoardPosition); }

void ATroup::getBoardPosition(int* line, int* col)
{
	*line = lineBoardPosition;
	*col = colBoardPosition;
}

void ATroup::setBoardPosition(TTuple<int, int> value) 
{ 
	lineBoardPosition = value.Get<0>();
	colBoardPosition = value.Get<1>();
}

void ATroup::setBoardPosition(int line, int col) 
{ 
	lineBoardPosition = line;
	colBoardPosition = col;
}
#pragma endregion


#pragma region HandleStatistics
int ATroup::getDamage()		{ return attackDamage; }

int ATroup::getDefence()	{ return defence; }

int ATroup::getLifePoint()	{ return healthPoint; }

void ATroup::increaseDecreaseDamage(int value)
{
	if (attackDamage + value < 0)
		attackDamage = 0;
	else
		attackDamage += value;
}

void ATroup::increaseDecreaseDefence(int value)
{
	if (defence + value < 0)
		defence = 0;
	else
		defence += value;
}

void ATroup::increaseDecreaseLife(int value)
{
	if (healthPoint + value > 0)
		healthPoint += value;
	else
		Destroy();
}
#pragma endregion

#pragma region Sound
void ATroup::playTroupSelectionSound() 
{
	switch (troupRace) 
	{
	case ETroupRace::KNIGHT:
		UGameplayStatics::PlaySound2D(this, knightTroupSelection);
		break;
	case ETroupRace::MONSTER:
		UGameplayStatics::PlaySound2D(this, monsterTroupSelection);
		break;
	case ETroupRace::SKELETON:
		UGameplayStatics::PlaySound2D(this, skeletonTroupSelection);
		break;
	}
}

void ATroup::playTileSelectionSound() 
{
	switch (troupRace) 
	{
	case ETroupRace::KNIGHT:
		UGameplayStatics::PlaySound2D(this, knightTileSelection);
		break;
	case ETroupRace::MONSTER:
		UGameplayStatics::PlaySound2D(this, monsterTileSelection);
		break;
	case ETroupRace::SKELETON:
		UGameplayStatics::PlaySound2D(this, skeletonTileSelection);
		break;
	}
}

void ATroup::playDeathSound()
{
	switch (troupRace)
	{
	case ETroupRace::KNIGHT:
		UGameplayStatics::PlaySound2D(this, knightDeath);
		break;
	case ETroupRace::MONSTER:
		UGameplayStatics::PlaySound2D(this, monsterDeath);
		break;
	case ETroupRace::SKELETON:
		UGameplayStatics::PlaySound2D(this, skeletonDeath);
		break;
	}
}
#pragma endregion

#pragma region Others
ETroupRace ATroup::getTroupRace()
{
	return troupRace;
}

ETroupRole ATroup::getTroupRole()
{
	return troupRole;
}

#pragma endregion
