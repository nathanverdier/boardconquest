// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/HopperBTTask_FindLocation.h"
#include "AIController.h"
#include "NavigationSystem.h"

UHopperBTTask_FindLocation::UHopperBTTask_FindLocation()
{
	NodeName = TEXT("Find a location");

	// accept only Vector
	BlackboardKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UHopperBTTask_FindLocation, BlackboardKey));
}

EBTNodeResult::Type UHopperBTTask_FindLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	FNavLocation Location{};
	 
	//Get AI Pawn
	AAIController* AIController{ OwnerComp.GetAIOwner() };
	const APawn* AIPawn{ AIController->GetPawn() };

	//Get Pawn origin
	const FVector Origin{ AIPawn->GetActorLocation() };

	// Obtain Navigation System and find a location
	const UNavigationSystemV1* NavSystem{UNavigationSystemV1::GetCurrent(GetWorld())};
	/*if (IsValid(NavSystem) && NavSystem->GetRandomPointInNavigableRadius(Origin, SearchRadius, Location));
	{
		UE_LOG(LogTemp, Log, TEXT("Test"));
		AIController->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.SelectedKeyName, Location.Location);
	}*/

	//Signal the BehaviorTreeComponent that the task finish with a sucess!
	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;

}

FString UHopperBTTask_FindLocation::GetStaticDescription() const
{
	return FString::Printf(TEXT("Vector: %s"), *BlackboardKey.SelectedKeyName.ToString());
}
