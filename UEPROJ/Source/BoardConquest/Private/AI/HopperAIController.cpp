// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/HopperAIController.h"
#include "BehaviorTree/BehaviorTree.h"

AHopperAIController::AHopperAIController()
{
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree Component"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackvoard Component"));
}

void AHopperAIController::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(BehavoirTree.Get())) {
		RunBehaviorTree(BehavoirTree.Get());
		BehaviorTreeComponent->StartTree(*BehavoirTree.Get());
	}
}

void AHopperAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (IsValid(Blackboard.Get()) && IsValid(BehavoirTree.Get())) {
		Blackboard->InitializeBlackboard(*BehavoirTree.Get()->BlackboardAsset.Get());
	}
}