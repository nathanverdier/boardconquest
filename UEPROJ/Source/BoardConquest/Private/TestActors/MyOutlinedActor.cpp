// Fill out your copyright notice in the Description page of Project Settings.


#include "TestActors/MyOutlinedActor.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AMyOutlinedActor::AMyOutlinedActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	IsOutlined = false;
}

// Called when the game starts or when spawned
void AMyOutlinedActor::BeginPlay()
{
	Super::BeginPlay();
	
	SetOutline(true);

	FTimerHandle TimerHandle;
	// Specify your function and parameters
	FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &AMyOutlinedActor::SetOutline, false);
	// Set timer for the function
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, 2.5, false);
}

// Called every frame
void AMyOutlinedActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyOutlinedActor::SetOutline(bool value)
{
	if (MeshComponent)
	{
		IsOutlined = value;
		MeshComponent->SetRenderCustomDepth(IsOutlined);
	}
}